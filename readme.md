# Computer Vision Assignment
This is the project for the assignments associated with Computer Vision.

### Prerequirements
- QtCreator
- QtCharts

### Project structure
    .
    ├── Component               This directory contains custom components for this project.
    ├── EdgeDetection           This directory contains the Edge Detection assignment.
    ├── Histogram               This directory contains the Histogram assignment.
    ├── ObjectSegmentation      This directory contains the Object Segmentation assignment.
    │   ├── Controller          This directory contains the controller functions for Object Segmentation.
    │   ├── Model               This directory contains the model functions for the Object Segmentation.
    │   └── View                This directory contains the view functions for the Object Segmentation.
    ├── Smoothing               This directory contains the Smoothing assignment.
    │   ├── Controller          This directory contains the controller functions for Smoothing.
    │   ├── Model               This directory contains the model functions for Smoothing.
    │   └── View                This directory contains the view functions for Smoothing.
    ├── Util                    This directory contains helper utils for image manipulation.
    ├── LICENSE
    └── README.md

### Contributors
- Darrel Griët - ACS

### License
This project uses the BSD license, more information about this can be found in the `LICENSE` file on the repository. Other parts of the application are licensed under their respective licenses (which are included in the source directories of those parts).
