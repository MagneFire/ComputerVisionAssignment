#include "ImageList.h"
#include <QScroller>
#include <QEasingCurve>
#include <QScrollBar>

ImageList::ImageList(const QString a_imagesFolder, QWidget* ) :
    imageFilesString(a_imagesFolder)
{
    setWidget(new QWidget());
    // Set the size policy.
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    // Set the minimum width.
    setMinimumWidth(148);
    // Remove frame.
    setFrameShape(Shape::NoFrame);
    // Enable resizable.
    setWidgetResizable(true);

    // Set a new layout.
    widget()->setLayout(new QFormLayout);
    // Set the content margins to zero.
    widget()->layout()->setContentsMargins(0, 0, 0, 0);
    // Set the horizontal spacing to zero.
    static_cast<QFormLayout*>(widget()->layout())->setHorizontalSpacing(0);
    // Set the vertical spacing to two.
    static_cast<QFormLayout*>(widget()->layout())->setVerticalSpacing(2);

#ifdef Q_OS_ANDROID
    // Enable scrolling use gestures.
    QScroller::grabGesture( this, QScroller::TouchGesture );
    // Disable scrollbars.
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
#endif

    // Add images.
    UpdateImages();

    // Enable drag and drop ability.
    setAcceptDrops(true);
}

void ImageList::UpdateImages()
{
    // Delete all widgets.
    while (widget()->layout()->count() > 0)
        // Remove widget.
        delete widget()->layout()->takeAt(0)->widget();

    QDirIterator imageFiles(imageFilesString,QDirIterator::Subdirectories);
    while (imageFiles.hasNext())
    {
        imageFiles.next();
        // Check if the current file/folder is a file.
        if (QFileInfo(imageFiles.filePath()).isFile())
        {
            // Create a new clickable label.
            ImageWidget *lblImage = new ImageWidget();
            // Disable built-in info dialog.
            lblImage->SetEnableInfoDialog(false);
            // Enable hovering, shows filename on mouse enter.
            lblImage->SetEnableHoverLabel(true);
            // Create new image.
            QImage * image = new QImage();
            // Load the file.
            image->load(imageFiles.filePath());

            // Connect the click signal to a click signal in this class.
            //connect(lblImage, SIGNAL(clicked(ImageWidget*,QMouseEvent*)), this, SLOT(ImageClicked(ImageWidget*,QMouseEvent*)));
            connect(lblImage, &ImageWidget::clicked, this, &ImageList::ImageClicked);
            lblImage->SetImage(*image);
            lblImage->filename = imageFiles.fileName();

            // Add the label with image to the images layout.
            widget()->layout()->addWidget(lblImage);
        }
    }
}

void ImageList::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasText())
        event->acceptProposedAction();
    else
        event->ignore();
}

void ImageList::dropEvent(QDropEvent *event)
{
    // Check if the images folder already exists, if not create one.
    if (!QDir(imageFilesString).exists())
        QDir().mkdir(imageFilesString);
    foreach (const QUrl &url, event->mimeData()->urls())
    {
        QString fileName = url.toLocalFile();
        QImage * image = new QImage();
        // Check if the file is a proper image.
        if(image->load(fileName))
        {
            // Copy the file to the images folder.
            QFile::copy(fileName, imageFilesString + "/" + url.fileName());
            // Update images.
            UpdateImages();
        }
        delete image;
    }
}
