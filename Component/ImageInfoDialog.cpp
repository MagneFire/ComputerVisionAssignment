#include "ImageInfoDialog.h"
#include "ui_ImageInfoDialog.h"
#include <QDebug>
#include <QDesktopWidget>
#include <QScreen>

ImageInfoDialog::ImageInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImageInfoDialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::WindowStaysOnTopHint);
    connect(ui->SaveButton, SIGNAL(clicked(bool)), this, SLOT(SaveImage()));

#ifdef Q_OS_ANDROID
    ui->horizontalLayout_3->removeItem(ui->horizontalSpacer_2);
    ui->horizontalLayout_3->removeItem(ui->horizontalSpacer_3);
#endif
}

ImageInfoDialog::~ImageInfoDialog()
{
    delete ui;
}

void ImageInfoDialog::SaveImage()
{
    // Check if the images folder already exists, if not create one.
    if (!QDir("generated").exists())
        QDir().mkdir("generated");
    ui->lblImage->pixmap()->toImage().save("generated/" + ui->lblTitle->text());
}

void ImageInfoDialog::SetTitle(const QString& title)
{
    ui->lblTitle->setText(title);
}
void ImageInfoDialog::SetImage(const QImage & image)
{
#ifdef Q_OS_ANDROID
    const QRect screen = QApplication::desktop()->screenGeometry();
    ui->lblImage->setPixmap(QPixmap::fromImage(image.scaled(screen.width(), screen.height(), Qt::KeepAspectRatio)));
#else
    ui->lblImage->setPixmap(QPixmap::fromImage(image));
#endif
    ui->lblImageSize->setText(QString::number(image.width()) + "x" + QString::number(image.height()));
}
