#ifndef IMAGEINFODIALOG_H
#define IMAGEINFODIALOG_H

#include <QDialog>
#include <QImage>
#include <QDir>

namespace Ui {
class ImageInfoDialog;
}

class ImageInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ImageInfoDialog(QWidget *parent = 0);
    ~ImageInfoDialog();

    void SetTitle(const QString&);
    void SetImage(const QImage &);
public slots:
    void SaveImage();

private:
    Ui::ImageInfoDialog *ui;
};

#endif // IMAGEINFODIALOG_H
