#include "ImageWidget.h"
#include <QDebug>
#include <QApplication>
#include <QDesktopWidget>

ImageWidget::ImageWidget(const QString& text, QWidget* parent)
    : QLabel(parent), topOffset(0), _EnableInfoDialog(false), _EnableHoverLabel(false), _Image(NULL), _scaled(NULL)
{
    setText(text);
    setMargin(0);
    installEventFilter(this);
}

ImageWidget::~ImageWidget()
{
}

void ImageWidget::SetImage(QImage & a_Image)
{
    SetImage(a_Image, 128, 128);
}
void ImageWidget::SetImage(QImage & a_Image, int width, int height)
{
    QImage * im = new QImage(a_Image.scaled(width, height));
    SetIcon(*im);
    SetImageSource(a_Image);
}

void ImageWidget::SetIcon(QImage & a_Image)
{
    _scaled = &a_Image;
    // Set actual visible icon.
    // Use scaled.
    setPixmap(QPixmap::fromImage(a_Image));
}

void ImageWidget::SetImageSource(QImage & a_Image)
{
    // Set image that is used externally.
    _Image = &a_Image;
    _ImageInfoDialog.SetImage(a_Image);
    // Adjust size if already shown.
    _ImageInfoDialog.adjustSize();
    // For some reason needs to be called twice.
    _ImageInfoDialog.adjustSize();
}
/*
void ImageWidget::SetImage(QImage & a_Image, bool)
{
    _Image = &a_Image;
    _scaled = &a_Image;
    setPixmap(QPixmap::fromImage(a_Image));
}*/

QImage * ImageWidget::GetImage()
{
    return _Image;
}

void ImageWidget::SetEnableInfoDialog(bool enable)
{
    _EnableInfoDialog = enable;
}

void ImageWidget::SetEnableHoverLabel(bool enable)
{
    _EnableHoverLabel = enable;
}

void ImageWidget::ShowLabel(bool enable)
{
    if(!enable)
    {
        // Set to default input image, if hover is enabled.
        if (_scaled != NULL)
            setPixmap(QPixmap::fromImage(*_scaled));
    }
    else
    {
        if (_scaled != NULL)
        {
            // Get the scaled image.
            QImage lblImage = *_scaled;//->scaled(128, 128);
            // tell the painter to draw on the QImage
            QPainter* painter = new QPainter(&lblImage);
            // Set the pen color to light gray.
            painter->setPen(QColor(220, 220, 220));
            // Set default font size and style.
            painter->setFont(QFont("Arial", 30));
            // Check if the filename might go out of bounce.
            float factor = ((double)lblImage.width()) / painter->fontMetrics().width(filename);
            if ((factor < 1))
            {
                QFont f = painter->font();
                // Adjust fontsize.
                f.setPointSizeF((int)((double)f.pointSizeF()*(double)factor));
                // Set the new fontsize.
                painter->setFont(f);
            }
            // Fill a rectangle that is dark gray.
            painter->fillRect(0, lblImage.height()*topOffset/100.0, lblImage.width(), 20, QBrush(QColor(100, 100, 100, 128)));
            // Draw the fileame to the image.
            painter->drawText(0, lblImage.height()*topOffset/100.0, lblImage.width(), 20, Qt::AlignCenter, filename);
            // Done painting to the image.
            painter->end();
            setPixmap(QPixmap::fromImage(lblImage));
        }
    }
}

bool ImageWidget::eventFilter(QObject *target, QEvent *event)
{

    if (target == this)
    {
        if(event->type() == QEvent::Leave)
        {
            if (_EnableHoverLabel)
                ShowLabel(false);
        }
        else if(event->type() == QEvent::Enter)
        {
            if (_EnableHoverLabel)
                ShowLabel(true);
        }
        else if ((event->type() == QEvent::MouseButtonPress) || (event->type() == QEvent::MouseButtonDblClick))
        {
            if (((((QMouseEvent*)event)->buttons() == Qt::RightButton) || (event->type() == QEvent::MouseButtonDblClick)) && _EnableInfoDialog)
            {
                _ImageInfoDialog.SetTitle(filename);

                _ImageInfoDialog.show();
                _ImageInfoDialog.adjustSize();
#ifdef Q_OS_ANDROID
                const QRect screen = QApplication::desktop()->screenGeometry();
                _ImageInfoDialog.move( screen.center() - _ImageInfoDialog.rect().center() );
#endif
            }

            emit clicked(static_cast<ImageWidget*>(target), (QMouseEvent*)event);
            return true;
        }
    }
    return false;
}
