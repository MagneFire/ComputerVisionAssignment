#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QLabel>
#include <QMouseEvent>
#include <QPainter>

#include <Component/ImageInfoDialog.h>

class ImageWidget : public QLabel
{
    Q_OBJECT
    public:
        explicit ImageWidget( const QString& text="", QWidget* parent=0 );
        ~ImageWidget();
    public:
        void SetImage(QImage &);
        void SetImage(QImage &, int, int);

        void SetIcon(QImage & a_Image);

        void SetImageSource(QImage & a_Image);
        QImage * GetImage();

        void SetEnableInfoDialog(bool enable);
        void SetEnableHoverLabel(bool enable);
        void ShowLabel(bool enable);

        uint8_t topOffset;
        QString filename;
        ImageInfoDialog _ImageInfoDialog;
    signals:
        void clicked(ImageWidget*, QMouseEvent*);
    protected:
        bool eventFilter(QObject *target, QEvent *event);
private:
        bool _EnableInfoDialog;
        bool _EnableHoverLabel;
        QImage * _Image;
        QImage * _scaled;
};

#endif // CLICKABLELABEL_H
