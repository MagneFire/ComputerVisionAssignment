#ifndef IMAGELIST_H
#define IMAGELIST_H

#include <QScrollArea>

#include "ImageWidget.h"
#include <QFileInfo>
#include <QDirIterator>
#include <QFormLayout>

#include <QDragEnterEvent>
#include <QMimeData>

#include <QDebug>

class ImageList : public QScrollArea
{
    Q_OBJECT
    public:
        explicit ImageList( const QString a_imagesFolder ,QWidget* parent=0 );
        inline ~ImageList() {}
    signals:
        void ImageClicked(ImageWidget * object, QMouseEvent * event);
    protected:
        void dragEnterEvent(QDragEnterEvent *event) Q_DECL_OVERRIDE;
        void dropEvent(QDropEvent *event) Q_DECL_OVERRIDE;
    private:
        void UpdateImages();

        const QString imageFilesString;
};

#endif // CLICKABLELABEL_H
