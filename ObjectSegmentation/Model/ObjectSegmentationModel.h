#ifndef OBJECTSEGMENTATIONMODEL_H
#define OBJECTSEGMENTATIONMODEL_H

#include <QObject>
#include <QImage>

class ObjectSegmentationModel : public QObject
{
    Q_OBJECT
public:

    enum ImageProcessingType
    {
        IMAGE_PROCESSING_TYPE_AVERAGE,
        IMAGE_PROCESSING_TYPE_MEDIAN
    };

    ObjectSegmentationModel();

    void SetInputImage(QImage & a_Image);
    QImage * GetInputImage();

    void SetBinaryImage(QImage & a_Image);
    QImage * GetBinaryImage();

    void SetDetectionImage(QImage & a_Image);
    QImage * GetDetectionImage();

    void SetInvertBinaryImage(bool invert);
    bool GetInvertBinaryImage();

    void SetResolution(uint8_t);
    uint8_t GetResolution();
signals:
    void DoImageProcessing(ObjectSegmentationModel::ImageProcessingType);
    void UpdateBinaryImage();
    void UpdateDetectionImage();
private:
    QImage *_InputImage;
    QImage *_BinaryImage;
    QImage *_DetectionImage;
    bool _invertBinaryImage;
    uint8_t _resolution;
};

#endif // SMOOTHINGMODEL_H
