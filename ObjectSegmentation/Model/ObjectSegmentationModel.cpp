#include "ObjectSegmentationModel.h"

#include <QDebug>


ObjectSegmentationModel::ObjectSegmentationModel():
    _invertBinaryImage(false),
    _resolution(0)
{

}

void ObjectSegmentationModel::SetInputImage(QImage & a_Image)
{
    _InputImage = &a_Image;
}

QImage * ObjectSegmentationModel::GetInputImage()
{
    return _InputImage;
}

void ObjectSegmentationModel::SetBinaryImage(QImage & a_Image)
{
    _BinaryImage = &a_Image;
    emit UpdateBinaryImage();
}

QImage * ObjectSegmentationModel::GetBinaryImage()
{
    return _BinaryImage;
}

void ObjectSegmentationModel::SetDetectionImage(QImage & a_Image)
{
    _DetectionImage = &a_Image;
    emit UpdateDetectionImage();
}

QImage * ObjectSegmentationModel::GetDetectionImage()
{
    return _DetectionImage;
}

void ObjectSegmentationModel::SetInvertBinaryImage(bool invert)
{
    _invertBinaryImage = invert;
}

bool ObjectSegmentationModel::GetInvertBinaryImage()
{
    return _invertBinaryImage;
}

void ObjectSegmentationModel::SetResolution(uint8_t resolution)
{
    _resolution = resolution;
}

uint8_t ObjectSegmentationModel::GetResolution()
{
    return _resolution;
}
