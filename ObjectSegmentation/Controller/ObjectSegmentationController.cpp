#include "ObjectSegmentationController.h"
#include <QDebug>


ObjectSegmentationController::ObjectSegmentationController(ObjectSegmentationModel & a_Model) :
    _Model(a_Model)
{

}

void ObjectSegmentationController::InputImageClicked(ImageWidget * object, QMouseEvent * event)
{
    if (event->buttons() == Qt::LeftButton)
    {
        qDebug() << "Performing Image Processing";
        _Model.SetInputImage(*object->GetImage());
        _Model.DoImageProcessing(ObjectSegmentationModel::IMAGE_PROCESSING_TYPE_AVERAGE);
    }
    else if (event->buttons() == Qt::RightButton)
    {
        _ImageInfoDialog.SetTitle(object->filename);
        _ImageInfoDialog.SetImage(*object->GetImage());
        _ImageInfoDialog.adjustSize();
        _ImageInfoDialog.show();
        _ImageInfoDialog.adjustSize();
    }
}
