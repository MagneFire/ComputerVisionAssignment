#ifndef OBJECTSEGMENTATIONCONTROLLER_H
#define OBJECTSEGMENTATIONCONTROLLER_H

#include <QObject>
#include <QMouseEvent>

#include "Component/ImageInfoDialog.h"

#include "ObjectSegmentation/Model/ObjectSegmentationModel.h"
#include "Component/ImageWidget.h"

class ObjectSegmentationController : public QObject
{
    Q_OBJECT
public:
    ObjectSegmentationController(ObjectSegmentationModel &);

private:
    ObjectSegmentationModel & _Model;
public slots:
    void InputImageClicked(ImageWidget *, QMouseEvent *);
private:
    ImageInfoDialog _ImageInfoDialog;
};

#endif // HISTOGRAMCONTROLLER_H
