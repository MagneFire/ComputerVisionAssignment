#include "ObjectSegmentationProcessing.h"

#include "Util/ImageUtils.h"
#include "Util/Utils.h"

#include <QDebug>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include <QPainter>
#include <QPen>

ObjectSegmentationProcessing::ObjectSegmentationProcessing(ObjectSegmentationModel & a_Model) :
    _Model(a_Model)
{
}

void ObjectSegmentationProcessing::DoImageProcessing(ObjectSegmentationModel::ImageProcessingType a_ImageProcessingType)
{
    _ImageProcessingType = a_ImageProcessingType;
    start();
}

QPoint _topLeft;
QPoint _bottomRight;

QImage * ObjectSegmentationProcessing::CreateBinaryImage(QImage * inputImage, bool invert, uint8_t threshold)
{
    QImage * _binaryImage;
    // Create a new output image.
    _binaryImage = new QImage(inputImage->width(), inputImage->height(), inputImage->format());


    QRgb * inputPixel;
    QRgb * binaryPixel;

    _topLeft.setX(_binaryImage->width() -1 );
    _topLeft.setY(_binaryImage->height() -1 );


    _bottomRight.setX(0);
    _bottomRight.setY(0);

    int stretchedPixel;

    for (int y = 0; y < _binaryImage->height(); y++)
    {
        inputPixel = reinterpret_cast<QRgb*>(inputImage->scanLine(y));
        binaryPixel = reinterpret_cast<QRgb*>(_binaryImage->scanLine(y));
        for (int x=0; x < inputImage->width(); x++)
        {
            stretchedPixel = 0;
            // Check if input pixel is below or above threshold value, depending on invert.
            if (((qGray(*(inputPixel + x)) < threshold) && !invert) || ((qGray(*(inputPixel + x)) > threshold) && invert))
            {
                stretchedPixel = 255;
                // Update the threshold boundries.
                if (_topLeft.x() >= x) _topLeft.setX(x);
                if (_topLeft.y() >= y) _topLeft.setY(y);

                if (_bottomRight.x() <= x) _bottomRight.setX(x);
                if (_bottomRight.y() <= y) _bottomRight.setY(y);
            }
            // Set the binary pixel in the binary image.
            *(binaryPixel++) = qRgb(stretchedPixel, stretchedPixel, stretchedPixel);
        }
    }
    return _binaryImage;
}

void ObjectSegmentationProcessing::DrawBoundryBox(QImage * image, QPoint topLeft, QPoint bottomRight)
{
    QPainter painter;
    QPen pen;
    // Set pen width to 4
    pen.setWidth(4);
    // Set pen color to red.
    pen.setColor(Qt::red);
    // Set painter for input image.
    painter.begin(image);
    // Set the pen to use for the image.
    painter.setPen(pen);
    // Draw a rectangle around the given coordinates.
    painter.drawRect(QRect(topLeft, bottomRight));
    // End painting.
    painter.end();
}

void ObjectSegmentationProcessing::run()
{
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    QImage * detectionImage = ImageUtils::CopyImage(_Model.GetInputImage(), false);

    qDebug() << "Starting image processing";
    _Model.SetBinaryImage(*CreateBinaryImage(_Model.GetInputImage(), _Model.GetInvertBinaryImage(), _Model.GetResolution()));
    DrawBoundryBox(detectionImage, _topLeft, _bottomRight);
    _Model.SetDetectionImage(*detectionImage);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    qDebug() << "Image processed in: "
             << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
             << "us";
}
