#ifndef OBJECTSEGMENTATIONPROCESSING_H
#define OBJECTSEGMENTATIONPROCESSING_H

#include <QObject>
#include <QThread>

#include "ObjectSegmentation/Model/ObjectSegmentationModel.h"


class ObjectSegmentationProcessing : public QThread
{
    Q_OBJECT
public:
    explicit ObjectSegmentationProcessing(ObjectSegmentationModel &);
signals:

public slots:
    void DoImageProcessing(ObjectSegmentationModel::ImageProcessingType);
private:
    /// Thread can only be started using given functions.
    void run();
    QImage * CreateBinaryImage(QImage * inputImage, bool invert, uint8_t threshold);
    void DrawBoundryBox(QImage * image, QPoint topLeft, QPoint bottomRight);
    ObjectSegmentationModel & _Model;
    ObjectSegmentationModel::ImageProcessingType _ImageProcessingType;



};

#endif // SMOOTHINGPROCESSING_H
