#ifndef OBJECTSEGMENTATIONVIEW_H
#define OBJECTSEGMENTATIONVIEW_H

#include <QMainWindow>

#include "ObjectSegmentation/Model/ObjectSegmentationModel.h"
#include "ObjectSegmentation/Controller/ObjectSegmentationController.h"

namespace Ui {
class ObjectSegmentationView;
}

class ObjectSegmentationView : public QMainWindow
{
    Q_OBJECT

public:
    explicit ObjectSegmentationView(ObjectSegmentationModel &, ObjectSegmentationController &);
    ~ObjectSegmentationView();
public slots:
    void UpdateBinaryImage();
    void UpdateDetectionImage();

    void ResolutionSliderChanged(int);
    void InvertBinaryImage(bool);

private:
    Ui::ObjectSegmentationView *ui;
    ObjectSegmentationModel & _Model;
};

#endif // SMOOTHINGVIEW_H
