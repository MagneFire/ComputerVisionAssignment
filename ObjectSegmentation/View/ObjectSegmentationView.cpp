#include "ObjectSegmentationView.h"
#include "ui_ObjectSegmentationView.h"

#include "Component/ImageWidget.h"
#include "Component/ImageList.h"

#include <QDirIterator>

#include <QDebug>

#include <QStandardPaths>

ImageWidget * _BinaryImage;
ImageWidget * _DetectionImage;

ObjectSegmentationView::ObjectSegmentationView(ObjectSegmentationModel & a_SmoothingModel, ObjectSegmentationController & a_SmoothingController) :
    QMainWindow(0),
    ui(new Ui::ObjectSegmentationView),
    _Model(a_SmoothingModel)
{
    ui->setupUi(this);

    ImageList * imageListInput = new ImageList(":/images/");
    connect(imageListInput, SIGNAL(ImageClicked(ImageWidget*,QMouseEvent*)), &a_SmoothingController, SLOT(InputImageClicked(ImageWidget*,QMouseEvent*)));

    ui->horizontalLayout->insertWidget(0, imageListInput);

    connect(ui->ResolutionSlider, SIGNAL(valueChanged(int)), this, SLOT(ResolutionSliderChanged(int)));

    connect(ui->cbInvertBinary, SIGNAL(clicked(bool)), this, SLOT(InvertBinaryImage(bool)));

    _BinaryImage = new ImageWidget();
    _BinaryImage->filename = "BinaryImage.png";
    _BinaryImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _BinaryImage->SetEnableInfoDialog(true);
    ui->horizontalLayout_2->addWidget(_BinaryImage);


    _DetectionImage = new ImageWidget();
    _DetectionImage->filename = "_DetectionImage.png";
    _DetectionImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _DetectionImage->SetEnableInfoDialog(true);
    ui->horizontalLayout_2->addWidget(_DetectionImage);

}

ObjectSegmentationView::~ObjectSegmentationView()
{
    delete ui;
}
void ObjectSegmentationView::ResolutionSliderChanged(int value)
{
    // Change the text resolution label.
    ui->ResolutionLabel->setText(QString::number(value) );
    _Model.SetResolution(value);
    _Model.DoImageProcessing(ObjectSegmentationModel::IMAGE_PROCESSING_TYPE_AVERAGE);
}
void ObjectSegmentationView::InvertBinaryImage(bool checked)
{
    _Model.SetInvertBinaryImage(checked);
    _Model.DoImageProcessing(ObjectSegmentationModel::IMAGE_PROCESSING_TYPE_AVERAGE);
}

void ObjectSegmentationView::UpdateBinaryImage()
{
    _BinaryImage->SetImage(*_Model.GetBinaryImage(), 256, 256);
}

void ObjectSegmentationView::UpdateDetectionImage()
{
    _DetectionImage->SetImage(*_Model.GetDetectionImage(), 256, 256);
}
