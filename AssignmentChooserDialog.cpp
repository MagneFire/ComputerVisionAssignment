#include "AssignmentChooserDialog.h"
#include "ui_AssignmentChooserDialog.h"
#include <QPushButton>
#include <QStyleFactory>
#include <QDesktopWidget>

AssignmentChooserDialog::AssignmentChooserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AssignmentChooserDialog),
    smoothingModel(NULL),
    smoothingController(NULL),
    smoothingProcessing(NULL),
    smoothingView(NULL),
    objectSegmentationModel(NULL),
    objectSegmentationController(NULL),
    objectSegmentationProcessing(NULL),
    objectSegmentationView(NULL),
    edgeDetectionModel(NULL),
    edgeDetectionView(NULL),
    morphologyModel(NULL),
    morphologyView(NULL),
    rleModel(NULL),
    rleView(NULL),
    feModel(NULL),
    feView(NULL),
    diceTossingModel(NULL),
    diceTossingView(NULL)
{
    ui->setupUi(this);

#ifdef Q_OS_ANDROID
    QImage * logoImag = new QImage();
    logoImag->load(":/logo/images/ComputerVision.png");
    ImageWidget *logoImage = new ImageWidget();
    logoImage->filename = "Computer Vision";
    logoImage->topOffset = 70;
    const QRect screen = QApplication::desktop()->screenGeometry();
    QImage im = logoImag->scaled(screen.width(), screen.height(), Qt::KeepAspectRatio);
    logoImage->SetIcon(im);
    logoImage->SetImageSource(*logoImag);
    logoImage->ShowLabel(true);
    ui->formLayout->insertRow(0, logoImage);
#endif

    connect(ui->btnHistogram, SIGNAL(clicked(bool)), this, SLOT(StartHistogram(bool)));
    connect(ui->btnObjectSegmentation, SIGNAL(clicked(bool)), this, SLOT(StartObjectSegmentation(bool)));
    connect(ui->btnSmoothing, SIGNAL(clicked(bool)), this, SLOT(StartSmoothing(bool)));
    connect(ui->btnEdgeDetection, SIGNAL(clicked(bool)), this, SLOT(StartEdgeDetection(bool)));
    connect(ui->btnMorphology, &QPushButton::clicked, this, &AssignmentChooserDialog::StartMorphology);
    connect(ui->btnRle, &QPushButton::clicked, this, &AssignmentChooserDialog::StartRle);
    connect(ui->btnFeatureExtraction, &QPushButton::clicked, this, &AssignmentChooserDialog::StartFe);
    connect(ui->btnDiceTossing, &QPushButton::clicked, this, &AssignmentChooserDialog::StartDiceTossing);
}

AssignmentChooserDialog::~AssignmentChooserDialog()
{
    delete ui;
}

void AssignmentChooserDialog::StartHistogram(bool)
{

}

void AssignmentChooserDialog::StartObjectSegmentation(bool)
{
    if (!objectSegmentationModel) objectSegmentationModel = new ObjectSegmentationModel();
    if (!objectSegmentationController) objectSegmentationController = new ObjectSegmentationController(*objectSegmentationModel);
    if (!objectSegmentationProcessing) objectSegmentationProcessing = new ObjectSegmentationProcessing(*objectSegmentationModel);
    if (!objectSegmentationView) objectSegmentationView = new ObjectSegmentationView(*objectSegmentationModel, *objectSegmentationController);

    QObject::connect(objectSegmentationModel, SIGNAL(DoImageProcessing(ObjectSegmentationModel::ImageProcessingType)), objectSegmentationProcessing, SLOT(DoImageProcessing(ObjectSegmentationModel::ImageProcessingType)));

    QObject::connect(objectSegmentationModel, SIGNAL(UpdateBinaryImage()), objectSegmentationView, SLOT(UpdateBinaryImage()));
    QObject::connect(objectSegmentationModel, SIGNAL(UpdateDetectionImage()), objectSegmentationView, SLOT(UpdateDetectionImage()));

    objectSegmentationView->show();
}

void AssignmentChooserDialog::StartSmoothing(bool)
{
    if (!smoothingModel) smoothingModel = new SmoothingModel();
    if (!smoothingController) smoothingController = new SmoothingController(*smoothingModel);
    if (!smoothingProcessing) smoothingProcessing = new SmoothingProcessing(*smoothingModel);
    if (!smoothingView) smoothingView = new SmoothingView(*smoothingModel, *smoothingController);

    QObject::connect(smoothingModel, SIGNAL(DoImageProcessing(SmoothingModel::ImageProcessingType)), smoothingProcessing, SLOT(DoImageProcessing(SmoothingModel::ImageProcessingType)));

    QObject::connect(smoothingModel, SIGNAL(UpdateAverageImage()), smoothingView, SLOT(UpdateAverageImage()));
    QObject::connect(smoothingModel, SIGNAL(UpdateMedianImage()), smoothingView, SLOT(UpdateMedianImage()));

    QObject::connect(smoothingModel, SIGNAL(UpdateAbsoluteAverageImage()), smoothingView, SLOT(UpdateAbsoluteAverageImage()));
    QObject::connect(smoothingModel, SIGNAL(UpdateAbsoluteMedianImage()), smoothingView, SLOT(UpdateAbsoluteMedianImage()));
    QObject::connect(smoothingModel, SIGNAL(UpdateManipulatedImage()), smoothingView, SLOT(UpdateManipulatedImage()));

    QObject::connect(smoothingModel, SIGNAL(UpdateInputImage()), smoothingView, SLOT(UpdateInputImage()));

    smoothingView->show();
}

void AssignmentChooserDialog::StartEdgeDetection(bool)
{
    if (!edgeDetectionModel) edgeDetectionModel = new EdgeDetectionModel();
    if (!edgeDetectionView) edgeDetectionView = new EdgeDetectionView(*edgeDetectionModel);

    // Connect signals and slots.

    edgeDetectionView->show();
}

void AssignmentChooserDialog::StartMorphology(bool)
{
    if (!morphologyModel) morphologyModel = new MorphologyModel();
    if (!morphologyView) morphologyView = new MorphologyView(*morphologyModel);

    morphologyView->show();
}

void AssignmentChooserDialog::StartRle()
{
    if (!rleModel) rleModel = new RleModel();
    if (!rleView) rleView = new RleView(*rleModel);

    rleView->show();
}

void AssignmentChooserDialog::StartFe()
{
    if (!feModel) feModel = new FeModel();
    if (!feView) feView = new FeView(*feModel);

    feView->show();
}

void AssignmentChooserDialog::StartDiceTossing()
{
    if (!diceTossingModel) diceTossingModel = new DiceTossingModel();
    if (!diceTossingView) diceTossingView = new DiceTossingView(*diceTossingModel);
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QStyle * android=QStyleFactory::create("Android");
    qApp->setStyle(android);
    AssignmentChooserDialog * assignmentChooserDialog = new AssignmentChooserDialog();

#ifdef Q_OS_ANDROID
    assignmentChooserDialog->showMaximized();
#else
    assignmentChooserDialog->show();
#endif

    return a.exec();
}
