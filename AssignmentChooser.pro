#-------------------------------------------------
#
# Project created by QtCreator 2016-06-05T19:41:53
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AssignmentChooser
TEMPLATE = app

unix:LIBS += -lopencv_objdetect -lopencv_imgcodecs -lopencv_videoio -lopencv_features2d -lopencv_imgproc -lopencv_highgui -lopencv_core


SOURCES +=  \
            Component/ImageInfoDialog.cpp \
            Component/ImageWidget.cpp \
            Component/ImageList.cpp \
            Component/CameraFrameGrabber.cpp \
            \
            Util/ImageUtils.cpp \
            Util/NoiseUtils.cpp \
            Util/Utils.cpp \
            Util/SmoothingUtils.cpp \
            \
            ObjectSegmentation/Model/ObjectSegmentationModel.cpp \
            ObjectSegmentation/Controller/ObjectSegmentationController.cpp\
            ObjectSegmentation/View/ObjectSegmentationView.cpp \
            ObjectSegmentation/ObjectSegmentationProcessing.cpp \
            \
            Smoothing/Model/SmoothingModel.cpp \
            Smoothing/Controller/SmoothingController.cpp\
            Smoothing/View/SmoothingView.cpp \
            Smoothing/SmoothingProcessing.cpp \
            \
            AssignmentChooserDialog.cpp \
            EdgeDetection/EdgeDetectionProcessing.cpp \
            EdgeDetection/View/EdgeDetectionView.cpp \
            EdgeDetection/Model/EdgeDetectionModel.cpp \
            \
            Morphology/MorphologyProcessing.cpp \
            Morphology/View/MorphologyView.cpp \
            Morphology/Model/MorphologyModel.cpp \
            Util/MorphologyUtils.cpp \
            \
            RLE/RleProcessing.cpp \
            RLE/View/RleView.cpp \
            RLE/Model/RleModel.cpp \
            Util/RleUtils.cpp \
            \
            FeatureExtraction/FeProcessing.cpp \
            FeatureExtraction/View/FeView.cpp \
            FeatureExtraction/Model/FeModel.cpp \
            Util/FeUtils.cpp \
            \
            DiceTossing/DiceTossingProcessing.cpp \
            DiceTossing/View/DiceTossingView.cpp \
            DiceTossing/Model/DiceTossingModel.cpp \
            Util/DiceTossingUtils.cpp

HEADERS  += \
            Component/ImageInfoDialog.h \
            Component/ImageWidget.h \
            Component/ImageList.h \
            Component/CameraFrameGrabber.h \
            \
            Util/ImageUtils.h \
            Util/NoiseUtils.h \
            Util/Utils.h \
            Util/SmoothingUtils.h \
            \
            ObjectSegmentation/Model/ObjectSegmentationModel.h \
            ObjectSegmentation/Controller/ObjectSegmentationController.h\
            ObjectSegmentation/View/ObjectSegmentationView.h \
            ObjectSegmentation/ObjectSegmentationProcessing.h \
            \
            Smoothing/Model/SmoothingModel.h \
            Smoothing/Controller/SmoothingController.h\
            Smoothing/View/SmoothingView.h \
            Smoothing/SmoothingProcessing.h \
            \
            AssignmentChooserDialog.h \
            EdgeDetection/EdgeDetectionProcessing.h \
            EdgeDetection/View/EdgeDetectionView.h \
            EdgeDetection/Model/EdgeDetectionModel.h \
            \
            Morphology/MorphologyProcessing.h \
            Morphology/View/MorphologyView.h \
            Morphology/Model/MorphologyModel.h \
            Util/MorphologyUtils.h \
            \
            RLE/RleProcessing.h \
            RLE/View/RleView.h \
            RLE/Model/RleModel.h \
            Util/RleUtils.h \
            \
            FeatureExtraction/FeProcessing.h \
            FeatureExtraction/View/FeView.h \
            FeatureExtraction/Model/FeModel.h \
            Util/FeUtils.h  \
            \
            DiceTossing/DiceTossingProcessing.h \
            DiceTossing/View/DiceTossingView.h \
            DiceTossing/Model/DiceTossingModel.h \
            Util/DiceTossingUtils.h

FORMS    += Component/ImageInfoDialog.ui \
            AssignmentChooserDialog.ui \
            ObjectSegmentation/View/ObjectSegmentationView.ui \
            Smoothing/View/SmoothingView.ui \
            EdgeDetection/View/EdgeDetectionView.ui \
            Morphology/View/MorphologyView.ui \
            RLE/View/RleView.ui \
            FeatureExtraction/View/FeView.ui \
            DiceTossing/View/DiceTossingView.ui

RESOURCES += \
    images.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
