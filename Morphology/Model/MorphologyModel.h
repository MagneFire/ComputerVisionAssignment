#ifndef MORPHOLOGYMODEL_H
#define MORPHOLOGYMODEL_H

#include <QObject>
class MorphologyProcessing;

class MorphologyModel : public QObject
{
    Q_OBJECT
public:
    enum ImageProcessingType
    {
        IMAGE_PROCESSING_TYPE_AVERAGE,
        IMAGE_PROCESSING_TYPE_GAUSSIAN,
        IMAGE_PROCESSING_TYPE_THRESHOLD,
        IMAGE_PROCESSING_TYPE_DILATION,
        IMAGE_PROCESSING_TYPE_EROSION,
        IMAGE_PROCESSING_TYPE_OPEN,
        IMAGE_PROCESSING_TYPE_CLOSE,
        IMAGE_PROCESSING_TYPE_ABSOLUTE
    };
    MorphologyModel();

    void SetInputImage(QImage &);
    inline QImage * GetInputImage()
    {
        return _InputImage;
    }

    void SetSmoothImage(QImage &);
    inline QImage * GetSmoothImage()
    {
        return _SmoothImage;
    }
    void SetThresholdImage(QImage &);
    inline QImage * GetThresholdImage()
    {
        return _ThresholdImage;
    }

    void SetScaledImage(QImage &);
    inline QImage * GetScaledImage()
    {
        return _ScaledImage;
    }
    void SetDilationImage(QImage &);
    inline QImage * GetDilationImage()
    {
        return _DilationImage;
    }
    void SetErosionImage(QImage &);
    inline QImage * GetErosionImage()
    {
        return _ErosionImage;
    }
    void SetOpeningImage(QImage &);
    inline QImage * GetOpeningImage()
    {
        return _OpeningImage;
    }
    void SetClosingImage(QImage &);
    inline QImage * GetClosingImage()
    {
        return _ClosingImage;
    }
    inline void SetDiffDilationImage(QImage &img)
    {
        _DiffDilationImage = &img;
        emit UpdateDiffDilationImage();
    }
    inline QImage * GetDiffDilationImage()
    {
        return _DiffDilationImage;
    }
    inline void SetDiffErosionImage(QImage &img)
    {
        _DiffErosionImage = &img;
        emit UpdateDiffErosionImage();
    }
    inline QImage * GetDiffErosionImage()
    {
        return _DiffErosionImage;
    }
    inline void SetDiffOpeningImage(QImage &img)
    {
        _DiffOpeningImage = &img;
        emit UpdateDiffOpenImage();
    }
    inline QImage * GetDiffOpeningImage()
    {
        return _DiffOpeningImage;
    }
    inline void SetDiffClosingImage(QImage &img)
    {
        _DiffClosingImage = &img;
        emit UpdateDiffCloseImage();
    }
    inline QImage * GetDiffClosingImage()
    {
        return _DiffClosingImage;
    }
    inline void SetThresHold(int threshold)
    {
        _threshold = threshold;
        // Update threshold image.
        SetSmoothImage(*_SmoothImage);
    }
    inline int GetThreshold() {return _threshold;}
    inline void SetScale(int scale)
    {
        _scale = scale;
        // Update threshold image.
        SetInputImage(*_InputImage);
    }
signals:
    void UpdateInputImage();
    void UpdateScaledImage();
    void UpdateSmoothImage();
    void UpdateThresholdImage();
    void UpdateDilationImage();
    void UpdateDiffDilationImage();
    void UpdateErosionImage();
    void UpdateDiffErosionImage();
    void UpdateOpenImage();
    void UpdateDiffOpenImage();
    void UpdateCloseImage();
    void UpdateDiffCloseImage();
private:
    QImage *_InputImage;
    QImage *_ScaledImage;
    QImage *_SmoothImage;
    QImage *_ThresholdImage;
    QImage *_DilationImage;
    QImage *_DiffDilationImage;
    QImage *_ErosionImage;
    QImage *_DiffErosionImage;
    QImage *_OpeningImage;
    QImage *_DiffOpeningImage;
    QImage *_ClosingImage;
    QImage *_DiffClosingImage;
    MorphologyProcessing* processing;
    int _threshold = 10;
    int _scale = 512;
};

#endif // EDGEDETECTIONMODEL_H
