#include "MorphologyModel.h"
#include "../MorphologyProcessing.h"

MorphologyModel::MorphologyModel():
    _InputImage(NULL),
    _SmoothImage(NULL),
    processing(new MorphologyProcessing(this))
{

}

void MorphologyModel::SetInputImage(QImage & input)
{
    int width = input.width();
    int height = input.height();
    _InputImage = &input;
    emit UpdateInputImage();
    QImage *tmpImage;
    if (width > height) tmpImage = new QImage(input.scaledToWidth(_scale));
    else _ScaledImage = tmpImage = new QImage(input.scaledToHeight(_scale));
    SetScaledImage(*tmpImage);
}

void MorphologyModel::SetScaledImage(QImage & img)
{
    _ScaledImage = &img;
    emit UpdateScaledImage();
    processing->DoImageProcessing(MorphologyModel::IMAGE_PROCESSING_TYPE_AVERAGE);
}

void MorphologyModel::SetSmoothImage(QImage & img)
{
    _SmoothImage = &img;
    emit UpdateSmoothImage();
    processing->DoImageProcessing(MorphologyModel::IMAGE_PROCESSING_TYPE_THRESHOLD);
}

void MorphologyModel::SetThresholdImage(QImage &img)
{
    _ThresholdImage = &img;
    emit UpdateThresholdImage();
    processing->DoImageProcessing(MorphologyModel::IMAGE_PROCESSING_TYPE_DILATION);
}

void MorphologyModel::SetDilationImage(QImage &img)
{
    _DilationImage = &img;
    emit UpdateDilationImage();
    processing->DoImageProcessing(MorphologyModel::IMAGE_PROCESSING_TYPE_EROSION);
}

void MorphologyModel::SetErosionImage(QImage &img)
{
    _ErosionImage = &img;
    emit UpdateErosionImage();
    processing->DoImageProcessing(MorphologyModel::IMAGE_PROCESSING_TYPE_OPEN);
}

void MorphologyModel::SetOpeningImage(QImage &img)
{
    _OpeningImage = &img;
    emit UpdateOpenImage();
    processing->DoImageProcessing(MorphologyModel::IMAGE_PROCESSING_TYPE_CLOSE);
}

void MorphologyModel::SetClosingImage(QImage &img)
{
    _ClosingImage = &img;
    emit UpdateCloseImage();
    processing->DoImageProcessing(MorphologyModel::IMAGE_PROCESSING_TYPE_ABSOLUTE);
}
