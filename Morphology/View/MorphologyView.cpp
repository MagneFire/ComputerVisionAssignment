#include "MorphologyView.h"
#include "ui_MorphologyView.h"
#include <QSlider>
#include <QCheckBox>

MorphologyView::MorphologyView(MorphologyModel & model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MorphologyView),
    model(model)
{
    ui->setupUi(this);


    // Allocate space for new image.
    QImage * tmpImage = new QImage(256, 256, QImage::Format_ARGB32);
    // Make the image transparent.
    tmpImage->fill(qRgba(0, 0, 0, 0));


    ImageList * imageListInput = new ImageList(":/images/");
    connect(imageListInput, &ImageList::ImageClicked, this, &MorphologyView::InputImageClicked);

    ui->ImageList->addWidget(imageListInput);

    _InputImage = new ImageWidget();
    _InputImage->filename = "InputImage.png";
    _InputImage->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    _InputImage->SetEnableInfoDialog(true);
    _InputImage->SetImage(*tmpImage, 64, 64);
    ui->ImageList->insertWidget(0, _InputImage);

    _SmoothImage = new ImageWidget();
    _SmoothImage->filename = "SmoothImage.png";
    _SmoothImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _SmoothImage->SetEnableInfoDialog(true);
    _SmoothImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH1->addWidget(_SmoothImage);

    _ThresholdImage = new ImageWidget();
    _ThresholdImage->filename = "ThresholdImage.png";
    _ThresholdImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _ThresholdImage->SetEnableInfoDialog(true);
    _ThresholdImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH1->addWidget(_ThresholdImage);


    _DilationImage = new ImageWidget();
    _DilationImage->filename = "DilationImage.png";
    _DilationImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _DilationImage->SetEnableInfoDialog(true);
    _DilationImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH2->addWidget(_DilationImage);

    _DiffDilationImage = new ImageWidget();
    _DiffDilationImage->filename = "DiffDilationImage.png";
    _DiffDilationImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _DiffDilationImage->SetEnableInfoDialog(true);
    _DiffDilationImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH3->addWidget(_DiffDilationImage);

    _ErosionImage = new ImageWidget();
    _ErosionImage->filename = "ErosionImage.png";
    _ErosionImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _ErosionImage->SetEnableInfoDialog(true);
    _ErosionImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH2->addWidget(_ErosionImage);

    _DiffErosionImage = new ImageWidget();
    _DiffErosionImage->filename = "DiffErosionImage.png";
    _DiffErosionImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _DiffErosionImage->SetEnableInfoDialog(true);
    _DiffErosionImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH3->addWidget(_DiffErosionImage);

    _ClosingImage = new ImageWidget();
    _ClosingImage->filename = "ClosingImage.png";
    _ClosingImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _ClosingImage->SetEnableInfoDialog(true);
    _ClosingImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH2->addWidget(_ClosingImage);

    _DiffClosingImage = new ImageWidget();
    _DiffClosingImage->filename = "DiffClosingImage.png";
    _DiffClosingImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _DiffClosingImage->SetEnableInfoDialog(true);
    _DiffClosingImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH3->addWidget(_DiffClosingImage);

    _OpeningImage = new ImageWidget();
    _OpeningImage->filename = "OpeningImage.png";
    _OpeningImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _OpeningImage->SetEnableInfoDialog(true);
    _OpeningImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH2->addWidget(_OpeningImage);

    _DiffOpeningImage = new ImageWidget();
    _DiffOpeningImage->filename = "DiffOpeningImage.png";
    _DiffOpeningImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _DiffOpeningImage->SetEnableInfoDialog(true);
    _DiffOpeningImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH3->addWidget(_DiffOpeningImage);

    connect(&model, &MorphologyModel::UpdateInputImage, this, &MorphologyView::UpdateInputImage, Qt::QueuedConnection);
    connect(&model, &MorphologyModel::UpdateSmoothImage, this, &MorphologyView::UpdateSmoothImage, Qt::QueuedConnection);
    connect(&model, &MorphologyModel::UpdateThresholdImage, this, &MorphologyView::UpdateThresholdImage, Qt::QueuedConnection);

    connect(&model, &MorphologyModel::UpdateDilationImage, this, &MorphologyView::UpdateDilationImage, Qt::QueuedConnection);
    connect(&model, &MorphologyModel::UpdateErosionImage, this, &MorphologyView::UpdateErosionImage, Qt::QueuedConnection);
    connect(&model, &MorphologyModel::UpdateOpenImage, this, &MorphologyView::UpdateOpenImage, Qt::QueuedConnection);
    connect(&model, &MorphologyModel::UpdateCloseImage, this, &MorphologyView::UpdateCloseImage, Qt::QueuedConnection);

    connect(&model, &MorphologyModel::UpdateDiffDilationImage, this, &MorphologyView::UpdateDiffDilationImage, Qt::QueuedConnection);
    connect(&model, &MorphologyModel::UpdateDiffErosionImage, this, &MorphologyView::UpdateDiffErosionImage, Qt::QueuedConnection);
    connect(&model, &MorphologyModel::UpdateDiffOpenImage, this, &MorphologyView::UpdateDiffOpenImage, Qt::QueuedConnection);
    connect(&model, &MorphologyModel::UpdateDiffCloseImage, this, &MorphologyView::UpdateDiffCloseImage, Qt::QueuedConnection);

    connect(ui->horizontalSlider, &QSlider::valueChanged, &model, &MorphologyModel::SetThresHold);
    connect(ui->scaleSlider, &QSlider::valueChanged, &model, &MorphologyModel::SetScale);
}

MorphologyView::~MorphologyView()
{
    delete ui;
}


void MorphologyView::InputImageClicked(ImageWidget * object, QMouseEvent * event)
{
    if (event->buttons() == Qt::LeftButton)
    {
        qDebug() << "Performing Image Processing";
        model.SetInputImage(*object->GetImage());
    }
    else if (event->buttons() == Qt::RightButton)
    {
        _ImageInfoDialog.SetTitle(object->filename);
        _ImageInfoDialog.SetImage(*object->GetImage());
        _ImageInfoDialog.adjustSize();
        _ImageInfoDialog.show();
        _ImageInfoDialog.adjustSize();
    }
}
