#ifndef MORPHOLOGYVIEW_H
#define MORPHOLOGYVIEW_H

#include <QWidget>

#include "Component/ImageWidget.h"
#include "Component/ImageList.h"

#include "../Model/MorphologyModel.h"
#include "../../Component/ImageInfoDialog.h"

namespace Ui {
class MorphologyView;
}

class MorphologyView : public QWidget
{
    Q_OBJECT

public:
    explicit MorphologyView(MorphologyModel & model, QWidget *parent = 0);
    ~MorphologyView();

    void InputImageClicked(ImageWidget * object, QMouseEvent * event);

    inline void UpdateInputImage()
    {
        _InputImage->SetImage(*model.GetInputImage(), 64, 64);
    }
    inline void UpdateSmoothImage()
    {
        _SmoothImage->SetImage(*model.GetSmoothImage(), 256, 256);
    }
    inline void UpdateDilationImage()
    {
        _DilationImage->SetImage(*model.GetDilationImage(), 256, 256);
    }
    inline void UpdateErosionImage()
    {
        _ErosionImage->SetImage(*model.GetErosionImage(), 256, 256);
    }
    inline void UpdateOpenImage()
    {
        _OpeningImage->SetImage(*model.GetOpeningImage(), 256, 256);
    }

    inline void UpdateDiffDilationImage()
    {
        _DiffDilationImage->SetImage(*model.GetDiffDilationImage(), 256, 256);
    }
    inline void UpdateDiffErosionImage()
    {
        _DiffErosionImage->SetImage(*model.GetDiffErosionImage(), 256, 256);
    }
    inline void UpdateDiffOpenImage()
    {
        _DiffOpeningImage->SetImage(*model.GetDiffOpeningImage(), 256, 256);
    }
    inline void UpdateDiffCloseImage()
    {
        _DiffClosingImage->SetImage(*model.GetDiffClosingImage(), 256, 256);
    }
    inline void UpdateCloseImage()
    {
        _ClosingImage->SetImage(*model.GetClosingImage(), 256, 256);
    }
    inline void UpdateThresholdImage()
    {
        _ThresholdImage->SetImage(*model.GetThresholdImage(), 256, 256);
    }
    inline void ThresholdChanged(int threshold)
    {
        model.SetThresHold(threshold);
    }

private:
    Ui::MorphologyView *ui;
    MorphologyModel & model;
    ImageWidget * _InputImage;
    ImageWidget * _SmoothImage;
    ImageWidget * _ThresholdImage;
    ImageWidget * _DilationImage;
    ImageWidget * _DiffDilationImage;
    ImageWidget * _ErosionImage;
    ImageWidget * _DiffErosionImage;
    ImageWidget * _OpeningImage;
    ImageWidget * _DiffOpeningImage;
    ImageWidget * _ClosingImage;
    ImageWidget * _DiffClosingImage;
    ImageInfoDialog _ImageInfoDialog;
};

#endif // EDGEDETECTIONVIEW_H
