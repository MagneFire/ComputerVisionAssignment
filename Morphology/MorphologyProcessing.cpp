#include "MorphologyProcessing.h"

MorphologyProcessing::MorphologyProcessing(MorphologyModel * model) :
      model(*model)
{

}

void MorphologyProcessing::DoImageProcessing(MorphologyModel::ImageProcessingType a_ImageProcessingType)
{
    if (model.GetInputImage() == NULL) return;
    _ImageProcessingType = a_ImageProcessingType;
    if (!isRunning())
    {
        start();
    }
}

void MorphologyProcessing::run()
{
    // Set new seed for rand().
    qsrand(QTime::currentTime().msec());
    double array[9];
    int total;
    MorphologyModel::ImageProcessingType type = _ImageProcessingType;

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    qDebug() << "Starting image processing";
    // Basically use a structuring element like this:
    //  0   1   0
    //  1   1   1
    //  0   1   0
    // Set default structuring element.
    array[0] = 0.0; array[1] = 1.0; array[2] = 0.0;
    array[3] = 1.0; array[4] = 1.0; array[5] = 1.0;
    array[6] = 0.0; array[7] = 1.0; array[8] = 0.0;

    do
    {
        // Update image processing type.
        type = _ImageProcessingType;
        switch(_ImageProcessingType)
        {
        case MorphologyModel::IMAGE_PROCESSING_TYPE_AVERAGE:
            array[0] = 1.0;
            array[1] = 1.0;
            array[2] = 1.0;
            array[3] = 1.0;
            array[4] = 1.0;
            array[5] = 1.0;
            array[6] = 1.0;
            array[7] = 1.0;
            array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set averaged smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case MorphologyModel::IMAGE_PROCESSING_TYPE_GAUSSIAN:
            array[0] = 1.0;
            array[1] = 2.0;
            array[2] = 1.0;
            array[3] = 2.0;
            array[4] = 4.0;
            array[5] = 2.0;
            array[6] = 1.0;
            array[7] = 2.0;
            array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set median smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case MorphologyModel::IMAGE_PROCESSING_TYPE_THRESHOLD:
            model.SetThresholdImage(*SmoothingUtils::GetThresholdImage(model.GetSmoothImage(), false, model.GetThreshold()));
            break;
        case MorphologyModel::IMAGE_PROCESSING_TYPE_DILATION:
            // Calculate Dilation. Using a 3x3(=9) size structuring element.
            model.SetDilationImage(*MorphologyUtils::GetMophologyImage(model.GetThresholdImage(), MorphologyUtils::IMAGE_PROCESSING_TYPE_DILATION, array, 9));
            break;
        case MorphologyModel::IMAGE_PROCESSING_TYPE_EROSION:
            // Calculate Erosion. Using a 3x3(=9) size structuring element.
            model.SetErosionImage(*MorphologyUtils::GetMophologyImage(model.GetThresholdImage(), MorphologyUtils::IMAGE_PROCESSING_TYPE_EROSION, array, 9));
            break;
        case MorphologyModel::IMAGE_PROCESSING_TYPE_OPEN:
            // Calculate Dilation after Erosion. (Opening) Using a 3x3(=9) size structuring element.
            model.SetOpeningImage(*MorphologyUtils::GetMophologyImage(model.GetErosionImage(), MorphologyUtils::IMAGE_PROCESSING_TYPE_DILATION, array, 9));
            break;
        case MorphologyModel::IMAGE_PROCESSING_TYPE_CLOSE:
            // Calculate Erosion after Dilation. (Closing) Using a 3x3(=9) size structuring element.
            model.SetClosingImage(*MorphologyUtils::GetMophologyImage(model.GetDilationImage(), MorphologyUtils::IMAGE_PROCESSING_TYPE_EROSION, array, 9));
            break;
        case MorphologyModel::IMAGE_PROCESSING_TYPE_ABSOLUTE:
            model.SetDiffDilationImage(*ImageUtils::GetAbsoluteImage(model.GetThresholdImage(), model.GetDilationImage()));
            model.SetDiffErosionImage(*ImageUtils::GetAbsoluteImage(model.GetThresholdImage(), model.GetErosionImage()));
            model.SetDiffOpeningImage(*ImageUtils::GetAbsoluteImage(model.GetThresholdImage(), model.GetOpeningImage()));
            model.SetDiffClosingImage(*ImageUtils::GetAbsoluteImage(model.GetThresholdImage(), model.GetClosingImage()));
            break;
        }
    } while (type != _ImageProcessingType);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    qDebug() << "Image processed in: "
             << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
             << "us";
}
