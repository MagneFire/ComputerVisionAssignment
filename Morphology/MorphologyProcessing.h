#ifndef MORPHOLOGYPROCESSING_H
#define MORPHOLOGYPROCESSING_H

#include <QDebug>
#include <QObject>
#include <QThread>
#include <QTime>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include "Model/MorphologyModel.h"
#include "../Util/MorphologyUtils.h"
#include "../Util/SmoothingUtils.h"
#include "../Util/Utils.h"
//class EdgeDetectionModel;

class MorphologyProcessing : public QThread
{
    Q_OBJECT
public:
    explicit MorphologyProcessing(MorphologyModel * model);
public slots:
    void DoImageProcessing(MorphologyModel::ImageProcessingType);
private:
    /// Thread can only be started using given functions.
    void run();
    MorphologyModel & model;
    MorphologyModel::ImageProcessingType _ImageProcessingType;
};

#endif // EDGEDETECTIONPROCESSING_H
