#ifndef FEUTILS_H
#define FEUTILS_H

#include <QDebug>
#include <QImage>

#include <QtMath>

#include "Utils.h"
#include "ImageUtils.h"

#define PIXEL_MAX 255
#define PIXEL_NOT_SET 150
#define PIXEL_MIN 0

class FeUtils : public QObject
{
    Q_OBJECT
private:
    explicit inline FeUtils() {}
public:
    typedef enum _MorphologyType
    {
        IMAGE_PROCESSING_TYPE_DILATION = 0x00,
        IMAGE_PROCESSING_TYPE_OPENING_DILATION_ONLY = 0x00,
        IMAGE_PROCESSING_TYPE_EROSION = 0x01,
        IMAGE_PROCESSING_TYPE_CLOSING_EROSION_ONLY = 0x01,
        IMAGE_PROCESSING_TYPE_OPENING = 0x02,
        IMAGE_PROCESSING_TYPE_CLOSING = 0x03
    } MorphologyType;

    static QImage * GetLabeledImage(QImage * inputImage, QRgb* labelsList, int maxLabels, int *labelCnt);
    static QImage * GetPImage(QImage * inputImage, QImage * processedImage, uint8_t value);
    static QImage * GetMophologyImage(QImage * inputImage, const MorphologyType &type, QRgb value, double* strucElement, uint8_t filterSize);
    //static QImage * GetMophologyImage(QImage * inputImage, const MorphologyType &type, double* strucElement, uint8_t filterSize);
    //static QImage * GetFilteredImage(QImage * inputImage, double* filter, uint8_t filterSize);
    //static QImage * GetDilationImage(QImage * inputImage, bool invert, double* strucElement, uint8_t filterSize);
public:
};

#endif // SMOOTHINGUTILS_H
