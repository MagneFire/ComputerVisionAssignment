#include "FeUtils.h"
const int dx[] = {+1, 0, -1, 0};
const int dy[] = {0, +1, 0, -1};

#define isSet(x) (x & 0xFFFFFF)

#define MAX_WIDTH 1024
#define MAX_HEIGHT 1024
#ifdef Q_OS_ANDROID
#define MAX_DEPTH 0x2000
#else
#define MAX_DEPTH 0x10000
#endif

uint8_t image[MAX_WIDTH][MAX_HEIGHT];
uint8_t labels[MAX_WIDTH][MAX_HEIGHT];
uint64_t depth;

void MarkNeigbours(int x, int y, int mx, int my, uint8_t label)
{
    if ((x < 0) || (x >= mx)) return;
    if ((y < 0) || (y >= my)) return;
    // Check if label is set.
    if (labels[x][y]) return;
    // Check if there is a proper label.
    if (!label) return;
    // Check if this is not a backgroud pixel.
    if (!image[x][y]) return;

    labels[x][y] = label;

    depth++;
    // Check if this is the max stack depth.
    if (depth > MAX_DEPTH) return;

    for (int d = 0; d < 4; d++)
      MarkNeigbours(x + dx[d], y + dy[d], mx, my, label);
}

QImage * FeUtils::GetLabeledImage(QImage *inputImage, QRgb* labelsList, int maxLabels, int * labelCnt)
{
    // Create a copy of the input image and make it gray if not already.
    QImage * labeledImage = new QImage(inputImage->copy());//ImageUtils::CopyImage(inputImage, false);
    memset(image, 0, sizeof(image[0][0]) * MAX_WIDTH * MAX_HEIGHT);
    memset(labels, 0, sizeof(labels[0][0]) * MAX_WIDTH * MAX_HEIGHT);
    int cnt = 1;
    if ((labeledImage->width() > MAX_WIDTH) || (labeledImage->height() > MAX_HEIGHT))
    {
        qDebug() << "Image is too big!";
    }

    for (int w = 0; w < labeledImage->width(); w++)
    {
        for (int h = 0; h < labeledImage->height(); h++)
        {
            image[w][h] = isSet(inputImage->pixel(w, h));
        }
    }
    for (int w = 0; w < labeledImage->width(); w++)
    {
        for (int h = 0; h < labeledImage->height(); h++)
        {
            if (qGray(inputImage->pixel(w, h)) == 255)
            {
                if (!(labels[w][h] & image[w][h]))
                {
                    depth = 0;
                    MarkNeigbours(w, h, labeledImage->width(), labeledImage->height(), cnt);
                    cnt++;
                }
            }
        }
    }

    // Loop through the width of the image.
    for (int w = 0; w < labeledImage->width(); w++)
    {
        // Loop throught the height of the image.
        for (int h = 0; h < labeledImage->height(); h++)
        {
            QColor color;
            int col = labels[w][h];
            col = ((double)(col-1) / (double)(cnt-2)) * 255.0;
            if (col <0) col = 0;
            if (labels[w][h] > 0)
            {
                color.setHsv(col, 255, 255);
                labeledImage->setPixel(w, h, color.rgba());
            }
        }
    }
    if ((cnt - 1) >= maxLabels) cnt = maxLabels;
    for (int i = 0; i < cnt-1; ++i)
    {
        QColor color;
        int col;
        col = ((double)(i) / (double)(cnt-2)) * 255.0;
        if (col <0) col = 0;
        color.setHsv(col, 255, 255);
        labelsList[i] = color.rgba();
    }
    *labelCnt = cnt-1;
    // Return the averaged image.
    return labeledImage;
}


QImage * FeUtils::GetPImage(QImage * inputImage, QImage * processedImage, uint8_t value)
{
    QImage * outputImage = new QImage(inputImage->width(), inputImage->height(), inputImage->format());

    QRgb * inputPixel;
    QRgb * medianPixel;
    QRgb * outputPixel;
    QRgb * endPixel;

    int pixel;

    // The image must be 32 bits.
    Q_ASSERT((inputImage->format() == QImage::Format_RGB32) || (inputImage->format() == QImage::Format_ARGB32));

    for (int y = 0; y < inputImage->height(); y++)
    {
        inputPixel = reinterpret_cast<QRgb*>(inputImage->scanLine(y));
        medianPixel = reinterpret_cast<QRgb*>(processedImage->scanLine(y));
        outputPixel = reinterpret_cast<QRgb*>(outputImage->scanLine(y));
        endPixel = inputPixel + inputImage->width();
        for (; inputPixel != endPixel; inputPixel++)
        {
            if (((uint8_t)qGray(*inputPixel) == value) && qGray(*medianPixel) == 0)
                pixel = 255;
            else
                pixel = 0;
            *(outputPixel++) = qRgb(pixel, pixel, pixel);
            medianPixel++;
        }
    }
    return outputImage;
}

QImage * FeUtils::GetMophologyImage(QImage * inputImage, const MorphologyType &type, QRgb value, double* strucElement, uint8_t filterSize)
{
    // Create a copy of the input image and make it gray if not already.
    QImage * morphologyImage = ImageUtils::CopyImage(inputImage, false);

    // Clear current image.
    for (int w = 0; w < inputImage->width(); w++)
        for (int h = 0; h < inputImage->height(); h++)
            morphologyImage->setPixel(w, h, qRgb(0, 0, 0));

    // Half resolution, e.g. 9 => 3x3 array.
    const int halfResolution = qSqrt(filterSize);
    // Quarter resolution, e.g. -1 to +1.
    const int quarterResolution = halfResolution / 2;
    uint8_t newValue = 0;
    // Create a hitlist.
    uint8_t *hitList = new uint8_t[filterSize];
    // Loop through the width of the image.
    for (int w = quarterResolution; w < inputImage->width() - quarterResolution; w++)
    {   // Loop throught the height of the image.
        for (int h = quarterResolution; h < inputImage->height() - quarterResolution; h++)
        {   // Clear the histlist.
            memset(hitList, PIXEL_NOT_SET, filterSize);
            // Loop through the surrounding pixels in the X axis.
            for (int i = -1 * quarterResolution; i <= quarterResolution; ++i)
            {   // Loop through the surrounding pixels in the Y axis.
                for (int j = -1 * quarterResolution; j <= quarterResolution; ++j)
                {   // Get array index from current pixel location.
                    int index = (((quarterResolution + i) * quarterResolution * 2) + (quarterResolution + i) + (quarterResolution + j));
                    // Check if the current pixel in the structuring element is used.
                    if (strucElement[index] > 0)
                    {   // Check if the pixel is black or white, and set the histlist variable as wel.
                        if (inputImage->pixel(w + i, h + j) == value)
                        {
                            //qDebug() << w + i << h + j << value << inputImage->pixel(w + i, h + j);
                            hitList[index] = PIXEL_MAX;
                        }
                        else
                            hitList[index] = PIXEL_MIN;
                    }
                }
            }
            if (type == IMAGE_PROCESSING_TYPE_DILATION)
                // Check if the maximum value in the array was white(hit), then set to this max value, else set black pixel.
                newValue = Utils::MaxArray(hitList, filterSize) == PIXEL_MAX ? PIXEL_MAX : PIXEL_MIN;
            else if (type == IMAGE_PROCESSING_TYPE_EROSION)
                // Check if the minimum value in the array was black(hit), then set to this min value, else set white pixel.
                newValue = Utils::MinArray(hitList, filterSize) == PIXEL_MIN ? PIXEL_MIN : PIXEL_MAX;
            morphologyImage->setPixel(w, h, qRgb(newValue, newValue, newValue));
        }
    }
    // Free memory from hitlist.
    delete hitList;
    // Return the averaged image.
    return morphologyImage;
}
