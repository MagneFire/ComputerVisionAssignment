#include "RleUtils.h"
#include <iostream>

uint32_t RleUtils::binaryImage[MAX_BINARY_SIZE];

uint32_t RleUtils::GetBinaryImage(QImage * inputImage, uint8_t threshold)
{
    // Set the index of the binary image to zero.
    uint32_t index = 0;
    // Set the count of the current value to zero.
    uint32_t count = 0;
    // Get the binary value of the pixel at position 0,0.
    uint32_t value = (qGray(inputImage->pixel(0, 0)) > threshold) ? 1 : 0;
    // Loop through the image, line by line.
    for (int h = 0; h < inputImage->height(); h++)
    {
        for (int w = 0; w < inputImage->width(); w++)
        {
            // Get the binary value of the current pixel.
            uint32_t newValue = (qGray(inputImage->pixel(w, h)) > threshold) ? 1 : 0;
            // Check if the current binary value differs from the previous one.
            if (value != newValue)
            {
                // Add the binary value to the binary image.
                // The count may be up to 31 bits, and the value is a single bit(i.e. 1 or 0).
                binaryImage[index] = (count << 1) | value;
                // Set new value.
                value = newValue;
                // Reset the value counter.
                count = 0;
                // Increment the binary image index.
                index++;
                // Make sure that there was no buffer overflow.
                if (index >= MAX_BINARY_SIZE)
                {
                    qDebug() << "Exceeded maximum binary image size!";
                    // return zero, to indicate failure.
                    return 0;
                }
            }
            else
                // Increment count.
                count ++;
        }
    }
    // Return the amount of data used.
    return index;
}

QImage * RleUtils::GetBinaryImage(uint32_t * inputImage, uint32_t width, uint32_t height)
{
    // Create a new image.
    QImage * outputImage = new QImage(width, height, QImage::Format_RGB32);

    // Clear current image.
    for (int w = 0; w < outputImage->width(); w++)
        for (int h = 0; h < outputImage->height(); h++)
            outputImage->setPixel(w, h, qRgb(0, 0, 0));
    // Reset the index for the binary input image.
    uint32_t index = 0;
    // Set the value from the first input image value.
    uint32_t value = (inputImage[0] & 0x01) ? 255 : 0;
    // Set the count from the first input image value.
    uint32_t count = inputImage[0] >> 1;
    // Loop throught the height of the image.
    for (uint h = 0; h < height; h++)
    {
        for (uint w = 0; w < width; w++)
        {
            if (count <= 0)
            {
                // Increment the current input image index.
                index ++;
                // get the next input image binary value.
                value = (inputImage[index] & 0x01) ? 255 : 0;
                // get the next input image count value.
                count = inputImage[index] >> 1;
            }
            else
                // Decrement the counter.
                count --;
            // Set the current pixel.
            outputImage->setPixel(w, h, qRgb(value, value, value));
        }
    }
    return outputImage;
}
