#ifndef SMOOTHINGUTILS_H
#define SMOOTHINGUTILS_H

#include <QDebug>
#include <QImage>

#include <QtMath>

#include "Utils.h"
#include "ImageUtils.h"

class SmoothingUtils : public QObject
{
    Q_OBJECT
private:
    explicit inline SmoothingUtils() {}
public:
    static QImage * GetThresholdImage(QImage * inputImage, bool invert, uint8_t threshold);
    static QImage * GetGradientImage(QImage * inputImage, QImage * inputImage2);
    static QImage * GetFilteredImage(QImage * inputImage, double* filter, uint8_t filterSize);
    static QImage * GetAveragedImage(QImage * inputImage, uint8_t resolution);
    static QImage * GetMedianImage(QImage * inputImage, uint8_t resolution);
public:
};

#endif // SMOOTHINGUTILS_H
