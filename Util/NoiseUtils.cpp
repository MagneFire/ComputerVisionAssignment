#include "NoiseUtils.h"

#include <QtMath>

NoiseUtils::NoiseUtils()
{
}

QImage * NoiseUtils::GetUniformNoiseImage(QImage * a_image, double magnitude)
{
    QImage * outputImage = new QImage(a_image->width(), a_image->height(), a_image->format());

    QRgb * inputPixel;
    QRgb * outputPixel;
    QRgb * endPixel;
    int grayPixel;

    // The image must be 32 bits.
    Q_ASSERT((a_image->format() == QImage::Format_RGB32) || (a_image->format() == QImage::Format_ARGB32));

    for (int y = 0; y < a_image->height(); y++)
    {
        // Go to the next line for the input pixel.
        inputPixel = reinterpret_cast<QRgb*>(a_image->scanLine(y));
        // Go to the next line for the output pixel.
        outputPixel = reinterpret_cast<QRgb*>(outputImage->scanLine(y));
        // Get the last pixel.
        endPixel = inputPixel + a_image->width();
        // Loop through all pixels.
        for (; inputPixel != endPixel; inputPixel++)
        {
            // Add noise to the input pixel.
            grayPixel = qGray(*inputPixel) + magnitude * (qrand() % 255);
            // Copy the pixel.
            *(outputPixel++) = qRgb(grayPixel, grayPixel, grayPixel);
        }
    }
    // Return the new image.
    return outputImage;
}

// Credits: http://c-faq.com/lib/gaussian.html
double gaussrand()
{
    static double V1, V2, S;
    static int phase = 0;
    double X;

    if(phase == 0) {
        do {
            double U1 = (double)qrand() / RAND_MAX;
            double U2 = (double)qrand() / RAND_MAX;

            V1 = 2 * U1 - 1;
            V2 = 2 * U2 - 1;
            S = V1 * V1 + V2 * V2;
            } while(S >= 1 || S == 0);

        X = V1 * sqrt(-2 * log(S) / S);
    } else
        X = V2 * sqrt(-2 * log(S) / S);

    phase = 1 - phase;

    return X;
}

QImage * NoiseUtils::GetGaussianNoiseImage(QImage * a_image, double deviation)
{
    QImage * outputImage = new QImage(a_image->width(), a_image->height(), a_image->format());

    QRgb * inputPixel;
    QRgb * outputPixel;
    QRgb * endPixel;
    int grayPixel;

    // The image must be 32 bits.
    Q_ASSERT((a_image->format() == QImage::Format_RGB32) || (a_image->format() == QImage::Format_ARGB32));

    for (int y = 0; y < a_image->height(); y++)
    {
        // Go to the next line for the input pixel.
        inputPixel = reinterpret_cast<QRgb*>(a_image->scanLine(y));
        // Go to the next line for the output pixel.
        outputPixel = reinterpret_cast<QRgb*>(outputImage->scanLine(y));
        // Get the last pixel.
        endPixel = inputPixel + a_image->width();
        // Loop through all pixels.
        for (; inputPixel != endPixel; inputPixel++)
        {
            // Add noise to the input pixel.
            //qDebug() << gaussrand();
            grayPixel = qGray(*inputPixel) + gaussrand() * deviation;// + magnitude * (qrand() % 255);
            // Copy the pixel.
            *(outputPixel++) = qRgb(grayPixel, grayPixel, grayPixel);
        }
    }
    // Return the new image.
    return outputImage;
}

QImage * NoiseUtils::GetSaltNPepperNoiseImage(QImage * a_image, double density)
{
    QImage * outputImage = new QImage(a_image->width(), a_image->height(), a_image->format());

    QRgb * inputPixel;
    QRgb * outputPixel;
    QRgb * endPixel;
    int grayPixel;

    // The image must be 32 bits.
    Q_ASSERT((a_image->format() == QImage::Format_RGB32) || (a_image->format() == QImage::Format_ARGB32));

    for (int y = 0; y < a_image->height(); y++)
    {
        // Go to the next line for the input pixel.
        inputPixel = reinterpret_cast<QRgb*>(a_image->scanLine(y));
        // Go to the next line for the output pixel.
        outputPixel = reinterpret_cast<QRgb*>(outputImage->scanLine(y));
        // Get the last pixel.
        endPixel = inputPixel + a_image->width();
        // Loop through all pixels.
        for (; inputPixel != endPixel; inputPixel++)
        {
            // Get a random number between 0 and 100;
            int randumNum = qrand() % 100;
            // Set gray pixel.
            grayPixel = qGray(*inputPixel);
            // Check if random number is below 100*density
            if (randumNum <= 100*density)
                // Replace pixel with a black or white pixel.
                grayPixel =  (randumNum <= 50*density) ? 255 : 0;
            // Copy the pixel.
            *(outputPixel++) = qRgb(grayPixel, grayPixel, grayPixel);
        }
    }
    // Return the new image.
    return outputImage;
}
