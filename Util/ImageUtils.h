#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

#include <QDebug>
#include <QImage>

class ImageUtils : public QObject
{
    Q_OBJECT
private:
    explicit ImageUtils();
public:
    static QImage * CopyImage(QImage * a_image, bool keepRGB);
    static QImage * GetAbsoluteImage(QImage * inputImage, QImage * processedImage);
};

#endif // IMAGEUTILS_H
