#include "Utils.h"

Utils::Utils()
{
}

void Utils::SortArray(int * array, int length)
{
    int higherValue;
    // Order array in incrementing order.
    for (int i = 0; i < length; ++i)
    {
        for (int j = i + 1; j < length; ++j)
        {
            if (array[i] > array[j])
            {
                higherValue =  array[i];
                array[i] = array[j];
                array[j] = higherValue;
            }
        }
    }
}

int Utils::SumArray(int * array, int length)
{
    int sum = 0;
    for (int i=0; i < length; i++)
        sum += array[i];

    return sum;
}

double Utils::SumArray(double * array, int length)
{
    double sum = 0;
    for (int i=0; i < length; i++)
        sum += array[i];

    return sum;
}

uint8_t Utils::MaxArray(uint8_t * array, int length)
{
    uint8_t max = 0;
    for (int i=0; i < length; i++)
        // Check if the current position is higher than the current max.
        if (array[i] > max) max = array[i];
    // Return maximum.
    return max;
}

uint8_t Utils::MinArray(uint8_t * array, int length)
{
    uint8_t min = 255;
    for (int i=0; i < length; i++)
        // Check if the current position is higher than the current min.
        if (array[i] < min) min = array[i];
    // Return minimum.
    return min;
}
