#ifndef RLEUTILS_H
#define RLEUTILS_H

#include <QDebug>
#include <QImage>

#include <QtMath>

#include "Utils.h"
#include "ImageUtils.h"

#define MAX_BINARY_SIZE 150000

class RleUtils : public QObject
{
    Q_OBJECT
private:
    explicit inline RleUtils() {}
public:
    static uint32_t binaryImage[MAX_BINARY_SIZE];
    static uint32_t GetBinaryImage(QImage * inputImage, uint8_t threshold);
    static QImage * GetBinaryImage(uint32_t * inputImage, uint32_t width, uint32_t height);
public:
};

#endif // SMOOTHINGUTILS_H
