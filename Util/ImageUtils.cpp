#include "ImageUtils.h"

ImageUtils::ImageUtils()
{
}

QImage * ImageUtils::CopyImage(QImage * a_image, bool keepRGB)
{
    QImage * outputImage = new QImage(a_image->width(), a_image->height(), a_image->format());

    QRgb * inputPixel;
    QRgb * outputPixel;
    QRgb * endPixel;

    // The image must be 32 bits.
    Q_ASSERT((a_image->format() == QImage::Format_RGB32) || (a_image->format() == QImage::Format_ARGB32));

    for (int y = 0; y < a_image->height(); y++)
    {
        inputPixel = reinterpret_cast<QRgb*>(a_image->scanLine(y));
        outputPixel = reinterpret_cast<QRgb*>(outputImage->scanLine(y));
        endPixel = inputPixel + a_image->width();
        // Loop through all pixels.
        for (; inputPixel != endPixel; inputPixel++)
        {
            // Copy the pixel.
            if (keepRGB)
                *(outputPixel++) = *inputPixel;
            else
                *(outputPixel++) = qRgb(qGray(*inputPixel), qGray(*inputPixel), qGray(*inputPixel));
        }
    }

    return outputImage;
}

QImage * ImageUtils::GetAbsoluteImage(QImage * inputImage, QImage * processedImage)
{
    QImage * outputImage = new QImage(inputImage->width(), inputImage->height(), inputImage->format());

    QRgb * inputPixel;
    QRgb * medianPixel;
    QRgb * outputPixel;
    QRgb * endPixel;

    int pixel;

    // The image must be 32 bits.
    Q_ASSERT((inputImage->format() == QImage::Format_RGB32) || (inputImage->format() == QImage::Format_ARGB32));

    for (int y = 0; y < inputImage->height(); y++)
    {
        inputPixel = reinterpret_cast<QRgb*>(inputImage->scanLine(y));
        medianPixel = reinterpret_cast<QRgb*>(processedImage->scanLine(y));
        outputPixel = reinterpret_cast<QRgb*>(outputImage->scanLine(y));
        endPixel = inputPixel + inputImage->width();
        for (; inputPixel != endPixel; inputPixel++)
        {
            pixel = abs(qGray(*inputPixel) - qGray(*medianPixel));
            *(outputPixel++) = qRgb(pixel, pixel, pixel);
            medianPixel++;
        }
    }
    return outputImage;
}
