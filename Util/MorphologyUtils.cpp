#include "MorphologyUtils.h"

QImage * MorphologyUtils::GetMophologyImage(QImage * inputImage, const MorphologyType &type, double* strucElement, uint8_t filterSize)
{
    // Create a copy of the input image and make it gray if not already.
    QImage * morphologyImage = ImageUtils::CopyImage(inputImage, false);

    // Clear current image.
    for (int w = 0; w < inputImage->width(); w++)
        for (int h = 0; h < inputImage->height(); h++)
            morphologyImage->setPixel(w, h, qRgb(0, 0, 0));

    // Half resolution, e.g. 9 => 3x3 array.
    const int halfResolution = qSqrt(filterSize);
    // Quarter resolution, e.g. -1 to +1.
    const int quarterResolution = halfResolution / 2;
    uint8_t newValue = 0;
    // Create a hitlist.
    uint8_t *hitList = new uint8_t[filterSize];
    // Loop through the width of the image.
    for (int w = quarterResolution; w < inputImage->width() - quarterResolution; w++)
    {   // Loop throught the height of the image.
        for (int h = quarterResolution; h < inputImage->height() - quarterResolution; h++)
        {   // Clear the histlist.
            memset(hitList, PIXEL_NOT_SET, filterSize);
            // Loop through the surrounding pixels in the X axis.
            for (int i = -1 * quarterResolution; i <= quarterResolution; ++i)
            {   // Loop through the surrounding pixels in the Y axis.
                for (int j = -1 * quarterResolution; j <= quarterResolution; ++j)
                {   // Get array index from current pixel location.
                    int index = (((quarterResolution + i) * quarterResolution * 2) + (quarterResolution + i) + (quarterResolution + j));
                    // Check if the current pixel in the structuring element is used.
                    if (strucElement[index] > 0)
                    {   // Check if the pixel is black or white, and set the histlist variable as wel.
                        hitList[index] = qGray(inputImage->pixel(w + i, h + j)) ? PIXEL_MAX : PIXEL_MIN;
                    }
                }
            }
            if (type == IMAGE_PROCESSING_TYPE_DILATION)
                // Check if the maximum value in the array was white(hit), then set to this max value, else set black pixel.
                newValue = Utils::MaxArray(hitList, filterSize) == PIXEL_MAX ? PIXEL_MAX : PIXEL_MIN;
            else if (type == IMAGE_PROCESSING_TYPE_EROSION)
                // Check if the minimum value in the array was black(hit), then set to this min value, else set white pixel.
                newValue = Utils::MinArray(hitList, filterSize) == PIXEL_MIN ? PIXEL_MIN : PIXEL_MAX;
            morphologyImage->setPixel(w, h, qRgb(newValue, newValue, newValue));
        }
    }
    // Free memory from hitlist.
    delete hitList;
    // Return the averaged image.
    return morphologyImage;
}


QImage * MorphologyUtils::GetDilationImage(QImage * inputImage, bool invert, double* strucElement, uint8_t filterSize)
{
    // Create a copy of the input image and make it gray if not already.
    QImage * dilationImage = ImageUtils::CopyImage(inputImage, false);

    // Clear current image.
    for (int w = 0; w < inputImage->width(); w++)
        for (int h = 0; h < inputImage->height(); h++)
            dilationImage->setPixel(w, h, qRgb(0, 0, 0));

    // Half resolution, e.g. 9 => 3x3 array.
    const int halfResolution = qSqrt(filterSize);
    // Quarter resolution, e.g. -1 to +1.
    const int quarterResolution = halfResolution / 2;
    double total = 0;
    // Loop through the width of the image.
    for (int w = quarterResolution; w < inputImage->width() - quarterResolution; w++)
    {
        // Loop throught the height of the image.
        for (int h = quarterResolution; h < inputImage->height() - quarterResolution; h++)
        {
            // Reset total value.
            total = 0;
            // Loop through the surrounding pixels in the X axis.
            for (int i = -1 * quarterResolution; i <= quarterResolution; ++i)
            {
                // Loop through the surrounding pixels in the Y axis.
                for (int j = -1 * quarterResolution; j <= quarterResolution; ++j)
                {
                    // Get array index from current pixel location.
                    int index = (((quarterResolution + i) * quarterResolution * 2) + (quarterResolution + i) + (quarterResolution + j));
                    if (strucElement[index] > 0)
                    {
                        if (total == 0)
                        {
                            if (invert)
                            {
                                if (qGray(inputImage->pixel(w + i, h + j)) < 255) total = 255;
                            }
                            else
                            {
                                if (qGray(inputImage->pixel(w + i, h + j)) > 0) total = 255;
                            }
                        }

                    }
                }
            }
            if (invert)
                total = 255 - total;
            // Update pixel.
            dilationImage->setPixel(w, h, qRgb(total, total, total));
        }
    }
    // Return the averaged image.
    return dilationImage;
}

QImage * MorphologyUtils::GetFilteredImage(QImage * inputImage, double* filter, uint8_t filterSize)
{
    // Create a copy of the input image and make it gray if not already.
    QImage * averagedImage = ImageUtils::CopyImage(inputImage, false);

    // Clear current image.
    for (int w = 0; w < averagedImage->width(); w++)
        for (int h = 0; h < averagedImage->height(); h++)
            averagedImage->setPixel(w, h, qRgb(0, 0, 0));

    // Half resolution, e.g. 9 => 3x3 array.
    const int halfResolution = qSqrt(filterSize);
    // Quarter resolution, e.g. -1 to +1.
    const int quarterResolution = halfResolution / 2;
    double total = 0;
    // Loop through the width of the image.
    for (int w = quarterResolution; w < averagedImage->width() - quarterResolution; w++)
    {
        // Loop throught the height of the image.
        for (int h = quarterResolution; h < averagedImage->height() - quarterResolution; h++)
        {
            // Reset total value.
            total = 0;
            // Loop through the surrounding pixels in the X axis.
            for (int i = -1 * quarterResolution; i <= quarterResolution; ++i)
            {
                // Loop through the surrounding pixels in the Y axis.
                for (int j = -1 * quarterResolution; j <= quarterResolution; ++j)
                {
                    // Get array index from current pixel location.
                    int index = (((quarterResolution + i) * quarterResolution * 2) + (quarterResolution + i) + (quarterResolution + j));
                    //index = jan;//(i + quarterResolution) + j;
                    // Add pixel value to the total pixel value.
                    total += qGray(inputImage->pixel(w + i, h + j)) * filter[index];
                }
            }
            // Update pixel.
            averagedImage->setPixel(w, h, qRgb(total, total, total));
        }
    }
    // Return the averaged image.
    return averagedImage;
}
