#include "SmoothingUtils.h"

#define IMG_MAX 1000


long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

QImage * SmoothingUtils::GetThresholdImage(QImage * inputImage, bool invert, uint8_t threshold)
{
    // Create a copy of the input image and make it gray if not already.
    QImage * averagedImage = ImageUtils::CopyImage(inputImage, false);;//new QImage(inputImage->copy());

    double total = 0;
    // Loop through the width of the image.
    for (int w = 0; w < averagedImage->width(); w++)
    {
        // Loop throught the height of the image.
        for (int h = 0; h < averagedImage->height(); h++)
        {
            total = qGray(inputImage->pixel(w, h));
            if ((total > threshold) && !invert) total = 255;
            else if ((total < threshold) && invert) total = 255;
            else total = 0;
            // Update the average pixel.
            averagedImage->setPixel(w, h, qRgb(total, total, total));
        }
    }
    // Return the averaged image.
    return averagedImage;
}

QImage * SmoothingUtils::GetFilteredImage(QImage * inputImage, double* filter, uint8_t filterSize)
{
    int max = 255;
    int min = 0;
    // Create a copy of the input image and make it gray if not already.
    QImage * filteredImage = new QImage(inputImage->copy());
    QImage * inImage = new QImage(inputImage->copy());
    // Loop through filter values.
    for (uint8_t i = 0; i < filterSize; ++i)
    {   // Check if the filter value will result in value boundries issues.
        if ((filter[i] > 1.0) || (filter[i] < 0.0))
        {   // Change the boundry.
            max = IMG_MAX;
            min = -IMG_MAX;
        }
    }
    // Half resolution, e.g. 9 => 3x3 array.
    const int halfResolution = qSqrt(filterSize);
    // Quarter resolution, e.g. -1 to +1.
    const int quarterResolution = halfResolution / 2;
    double total = 0;
    // Loop through the width of the image.
    for (int w = quarterResolution; w < filteredImage->width() - quarterResolution; w++)
    {   // Loop throught the height of the image.
        for (int h = quarterResolution; h < filteredImage->height() - quarterResolution; h++)
        {   // Reset total value.
            total = 0;
            // Loop through the surrounding pixels in the X axis.
            for (int i = -1 * quarterResolution; i <= quarterResolution; ++i)
            {   // Loop through the surrounding pixels in the Y axis.
                for (int j = -1 * quarterResolution; j <= quarterResolution; ++j)
                {   // Get array index from current pixel location.
                    int index = (((quarterResolution + i) * quarterResolution * 2) + (quarterResolution + i) + (quarterResolution + j));
                    // Add pixel value to the total pixel value.
                    total += qGray(inImage->pixel(w + i, h + j)) * filter[index];
                }
            }
            // Only map when needed.
            if (max != 255) total = map(total, min, max, 0, 255);
            // Update pixel.
            filteredImage->setPixel(w, h, qRgb(total, total, total));
        }
    }
    delete inImage;
    // Return the averaged image.
    return filteredImage;
}

QImage * SmoothingUtils::GetGradientImage(QImage * inputImage, QImage * inputImage2)
{
    // Create a copy of the input image and make it gray if not already.
    QImage * gradientImage = new QImage(inputImage->copy());
    // Image data array.
    int* dataArray = new int[gradientImage->width() * gradientImage->height()];

    int total = 0;
    // Set top to minimal.
    int top = -IMG_MAX;
    // Set bottom to maximum.
    int bottom = IMG_MAX;
    // Loop through the width of the image.
    for (int w = 0; w < gradientImage->width(); w++)
    {
        // Loop throught the height of the image.
        for (int h = 0; h < gradientImage->height(); h++)
        {
            // Get the input pixel from image 1.
            int i1 = qGray(inputImage->pixel(w, h));
            // Get the input pixel from image 2.
            int i2 = qGray(inputImage2->pixel(w, h));
            // Scale the input pixel back to the original.
            i1  = map(i1, 0, 255, -IMG_MAX, IMG_MAX);
            // Scale the input pixel back to the original.
            i2  = map(i2, 0, 255, -IMG_MAX, IMG_MAX);
            // Do gradient magnitude calculation.
            total = qSqrt(qPow(i1,2)+ qPow(i2, 2));
            // Update value in image data array.
            dataArray[w*gradientImage->height() + h] = total;
            // Check if this is the maximum value.
            if (top < total) top = total;
            // Check if this is the minimal value.
            if (bottom > total) bottom = total;
        }
    }
    // Loop through the width of the image.
    for (int w = 0; w < gradientImage->width(); w++)
    {
        // Loop throught the height of the image.
        for (int h = 0; h < gradientImage->height(); h++)
        {
            // Map image data back to QImage compatible format.
            dataArray[w*gradientImage->height() + h] = map(dataArray[w*gradientImage->height() + h], bottom, top, 0, 255);
            // Update the average pixel.
            gradientImage->setPixel(w, h, qRgb(dataArray[w*gradientImage->height() + h], dataArray[w*gradientImage->height() + h], dataArray[w*gradientImage->height() + h]));
        }
    }
    delete dataArray;
    // Return the averaged image.
    return gradientImage;
}

QImage * SmoothingUtils::GetAveragedImage(QImage * inputImage, uint8_t resolution)
{
    // Make sure that resolution is uneven.
    if ((resolution % 2) == 0) resolution++;
    // Calculate full grid size
    const int fullResolution = resolution * resolution;
    double array[fullResolution];
    for (uint8_t i=0; i < fullResolution; ++i)
        array[i] = 1.0 / fullResolution;
    // Create a copy of the input image and make it gray if not already.
    QImage * averagedImage = ImageUtils::CopyImage(inputImage, false);

    // Half resolution, e.g. 9 => 3x3 array.
    const int halfResolution = qSqrt(fullResolution);
    // Quarter resolution, e.g. -1 to +1.
    const int quarterResolution = halfResolution / 2;
    double total = 0;
    // Loop through the width of the image.
    for (int w = quarterResolution; w < averagedImage->width() - quarterResolution; w++)
    {
        // Loop throught the height of the image.
        for (int h = quarterResolution; h < averagedImage->height() - quarterResolution; h++)
        {
            // Reset total value.
            total = 0;
            // Loop through the surrounding pixels in the X axis.
            for (int i = -1 * quarterResolution; i <= quarterResolution; ++i)
            {
                // Loop through the surrounding pixels in the Y axis.
                for (int j = -1 * quarterResolution; j <= quarterResolution; ++j)
                {
                    // Get array index from current pixel location.
                    int index = (((quarterResolution + i) * quarterResolution * 2) + (quarterResolution + i) + (quarterResolution + j));
                    // Add pixel value to the total pixel value.
                    total += qGray(inputImage->pixel(w + i, h + j)) * array[index];
                }
            }
            total = qAbs(total);
            if (total > 255) total = 255;
            // Update the average pixel.
            averagedImage->setPixel(w, h, qRgb(total, total, total));
        }
    }
    // Return the averaged image.
    return averagedImage;
}

QImage * SmoothingUtils::GetMedianImage(QImage * inputImage, uint8_t resolution)
{
    // Create a copy of the input image and make it gray if not already.
    QImage * medianImage = ImageUtils::CopyImage(inputImage, false);

    const int halfResolution = resolution / 2;
    const int fullResolution = resolution * resolution;

    // Array to store the pixels in.
    int array[fullResolution];
    int total = 0;
    // Loop through the width of the image.
    for (int w = halfResolution; w < medianImage->width() - halfResolution; w++)
    {   // Loop through the height the image.
        for (int h = halfResolution; h < medianImage->height() - halfResolution; h++)
        {   // Loop through the surrounding pixels in the X axis.
            for (int i = -1 * halfResolution; i <= halfResolution; ++i)
            {   // Loop through the surrounding pixels in the Y axis.
                for (int j = -1 * halfResolution; j <= halfResolution; ++j)
                {   // Calculate index of array and set the pixel.
                    array[(((halfResolution + i) * halfResolution*2) + (halfResolution + i) + (halfResolution + j))] = qGray(medianImage->pixel(w + i, h + j));
                }
            }
            // Sort the array.
            Utils::SortArray(array, fullResolution);
            // Get the median value.
            total = array[(fullResolution/2)];
            // Set the new pixel value.
            medianImage->setPixel(w, h, qRgb(total, total, total));
        }
    }
    // Return the median image.
    return medianImage;
}
