#ifndef NOISEUTILS_H
#define NOISEUTILS_H

#include <QDebug>
#include <QImage>

#include "ImageUtils.h"

class NoiseUtils : public QObject
{
    Q_OBJECT
private:
    explicit NoiseUtils();
public:
    static QImage * GetUniformNoiseImage(QImage * a_image, double magnitude);
    static QImage * GetGaussianNoiseImage(QImage * a_image, double deviation);
    static QImage * GetSaltNPepperNoiseImage(QImage * a_image, double density);
};

#endif // NOISEUTILS_H
