#ifndef UTILS_H
#define UTILS_H

#include <QDebug>

class Utils : public QObject
{
    Q_OBJECT
private:
    explicit Utils();
public:
    static void SortArray(int * array, int length);
    static int SumArray(int * array, int length);
    static double SumArray(double * array, int length);
    static uint8_t MaxArray(uint8_t * array, int length);
    static uint8_t MinArray(uint8_t * array, int length);
};

#endif // UTILS_H
