#ifndef FEPROCESSING_H
#define FEPROCESSING_H

#include <QDebug>
#include <QObject>
#include <QThread>
#include <QTime>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include "Model/FeModel.h"

#include "../Util/MorphologyUtils.h"
#include "../Util/RleUtils.h"
#include "../Util/FeUtils.h"
#include "../Util/SmoothingUtils.h"
#include "../Util/RleUtils.h"
#include "../Util/Utils.h"
//class EdgeDetectionModel;
#include <QCamera>
#include "../Component/CameraFrameGrabber.h"

class FeProcessing : public QThread
{
    Q_OBJECT
private:
    QCamera * _camera;
    CameraFrameGrabber * _camGrabber;
public:
    explicit FeProcessing(FeModel * model);
public slots:
    void DoImageProcessing(FeModel::ImageProcessingType);
    void EnableCameraInput(bool enable);
private:
    /// Thread can only be started using given functions.
    void run();
    void frameUpdated(QImage ime);
    FeModel & model;
    FeModel::ImageProcessingType _ImageProcessingType;
};

#endif // EDGEDETECTIONPROCESSING_H
