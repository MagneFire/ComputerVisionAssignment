#ifndef FEVIEW_H
#define FEVIEW_H

#include <QWidget>

#include "Component/ImageWidget.h"
#include "Component/ImageList.h"

#include "../Model/FeModel.h"
#include "../../Component/ImageInfoDialog.h"

namespace Ui {
class FeView;
}

class FeView : public QWidget
{
    Q_OBJECT

public:
    explicit FeView(FeModel & model, QWidget *parent = 0);
    ~FeView();

    void InputImageClicked(ImageWidget * object, QMouseEvent * event);

    inline void UpdateInputImage()
    {
        _InputImage->SetImage(*model.GetInputImage(), 64, 64);
    }
    inline void UpdateLabeledImage()
    {
        _LabeledImage->SetImage(*model.GetLabeledImage(), 512, 512);
    }
    inline void UpdateLegendImage()
    {
        _LegendImage->SetImage(*model.GetLegendImage(), 200, 512);
    }

    inline void UpdateThresholdImage()
    {
        _ThresholdImage->SetImage(*model.GetThresholdImage(), 512, 512);
    }
    void UpdateLabelCount();
    void UpdateCircleCount();

    inline void ThresholdChanged(int threshold)
    {
        model.SetThresHold(threshold);
    }

private:
    Ui::FeView *ui;
    FeModel & model;
    ImageWidget * _InputImage;
    ImageWidget * _LabeledImage;
    ImageWidget * _LegendImage;
    ImageWidget * _ThresholdImage;
    ImageInfoDialog _ImageInfoDialog;
};

#endif // EDGEDETECTIONVIEW_H
