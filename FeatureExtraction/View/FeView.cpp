#include "FeView.h"
#include "ui_FeView.h"
#include <QSlider>
#include <QCheckBox>

FeView::FeView(FeModel & model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FeView),
    model(model)
{
    ui->setupUi(this);


    // Allocate space for new image.
    QImage * tmpImage = new QImage(256, 256, QImage::Format_ARGB32);
    // Make the image transparent.
    tmpImage->fill(qRgba(0, 0, 0, 0));


    ImageList * imageListInput = new ImageList(":/images/");
    connect(imageListInput, &ImageList::ImageClicked, this, &FeView::InputImageClicked);

    ui->ImageList->addWidget(imageListInput);

    _InputImage = new ImageWidget();
    _InputImage->filename = "InputImage.png";
    _InputImage->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    _InputImage->SetEnableInfoDialog(true);
    _InputImage->SetImage(*tmpImage, 64, 64);
    ui->ImageList->insertWidget(0, _InputImage);

    _ThresholdImage = new ImageWidget();
    _ThresholdImage->filename = "ThresholdImage.png";
    _ThresholdImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _ThresholdImage->SetEnableInfoDialog(true);
    _ThresholdImage->SetImage(*tmpImage, 512, 512);
    ui->LayoutH1->addWidget(_ThresholdImage);

    _LabeledImage = new ImageWidget();
    _LabeledImage->filename = "LabeledImage.png";
    _LabeledImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _LabeledImage->SetEnableInfoDialog(true);
    _LabeledImage->SetImage(*tmpImage, 512, 512);
    ui->LayoutH1->addWidget(_LabeledImage);

    _LegendImage = new ImageWidget();
    _LegendImage->filename = "LegendImage.png";
    _LegendImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _LegendImage->SetEnableInfoDialog(true);
    _LegendImage->SetImage(*tmpImage, 200, 512);
    ui->LayoutH1->addWidget(_LegendImage);

    connect(&model, &FeModel::UpdateInputImage, this, &FeView::UpdateInputImage, Qt::QueuedConnection);
    connect(&model, &FeModel::UpdateThresholdImage, this, &FeView::UpdateThresholdImage, Qt::QueuedConnection);
    connect(&model, &FeModel::UpdateLabeledImage, this, &FeView::UpdateLabeledImage, Qt::QueuedConnection);
    connect(&model, &FeModel::UpdateLegendImage, this, &FeView::UpdateLegendImage, Qt::QueuedConnection);
    connect(&model, &FeModel::UpdateLabelCount, this, &FeView::UpdateLabelCount, Qt::QueuedConnection);
    connect(&model, &FeModel::UpdateCircleCount, this, &FeView::UpdateCircleCount, Qt::QueuedConnection);

    connect(ui->horizontalSlider, &QSlider::valueChanged, &model, &FeModel::SetThresHold);
    connect(ui->scaleSlider, &QSlider::valueChanged, &model, &FeModel::SetScale);
    connect(ui->cbInvert, &QCheckBox::clicked, &model, &FeModel::SetInvert);
    connect(ui->cbCamera, &QCheckBox::clicked, &model, &FeModel::EnableCameraInput);
}

FeView::~FeView()
{
    delete ui;
}

void FeView::UpdateLabelCount()
{
    ui->lblCount->setText("Labels: "+QString::number(model.GetLabelCount()));
}

void FeView::UpdateCircleCount()
{
    ui->lblCircles->setText("Circles: "+QString::number(model.GetCircleCount()));
}

void FeView::InputImageClicked(ImageWidget * object, QMouseEvent * event)
{
    if (event->buttons() == Qt::LeftButton)
    {
        qDebug() << "Performing Image Processing";
        model.SetInputImage(*object->GetImage());
    }
    else if (event->buttons() == Qt::RightButton)
    {
        _ImageInfoDialog.SetTitle(object->filename);
        _ImageInfoDialog.SetImage(*object->GetImage());
        _ImageInfoDialog.adjustSize();
        _ImageInfoDialog.show();
        _ImageInfoDialog.adjustSize();
    }
}
