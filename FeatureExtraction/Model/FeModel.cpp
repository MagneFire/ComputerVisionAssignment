#include "FeModel.h"
#include "../FeProcessing.h"

FeModel::FeModel():
    _InputImage(NULL),
    _ScaledImage(NULL),
    _SmoothImage(NULL),
    _ThresholdImage(NULL),
    _LabeledImage(NULL),
    _LegendImage(NULL),
    _labelCount(0),
    processing(new FeProcessing(this)),
    invert(false)
{
}

void FeModel::SetInputImage(QImage & input)
{
    int width = input.width();
    int height = input.height();
    _InputImage = &input;
    emit UpdateInputImage();
    QImage *tmpImage;
    if (width > height) tmpImage = new QImage(input.scaledToWidth(_scale));
    else _ScaledImage = tmpImage = new QImage(input.scaledToHeight(_scale));
    if (GetScaledImage() != NULL)
    {
        delete GetScaledImage();
    }
    SetScaledImage(*tmpImage);
}

void FeModel::SetScaledImage(QImage & img)
{
    _ScaledImage = &img;
    emit UpdateScaledImage();
    processing->DoImageProcessing(FeModel::IMAGE_PROCESSING_TYPE_BINARY);
}

void FeModel::SetSmoothImage(QImage & img)
{
    _SmoothImage = &img;
    emit UpdateSmoothImage();
    processing->DoImageProcessing(FeModel::IMAGE_PROCESSING_TYPE_THRESHOLD);
}

void FeModel::SetThresholdImage(QImage &img)
{
    _ThresholdImage = &img;
    emit UpdateThresholdImage();
    processing->DoImageProcessing(FeModel::IMAGE_PROCESSING_TYPE_LABELED);
}

void FeModel::EnableCameraInput(bool enable)
{
    processing->EnableCameraInput(enable);
}
