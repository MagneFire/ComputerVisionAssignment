#ifndef FEMODEL_H
#define FEMODEL_H

#include <QObject>
class FeProcessing;

class FeModel : public QObject
{
    Q_OBJECT
public:
    enum ImageProcessingType
    {
        IMAGE_PROCESSING_TYPE_BINARY,
        IMAGE_PROCESSING_TYPE_AVERAGE,
        IMAGE_PROCESSING_TYPE_GAUSSIAN,
        IMAGE_PROCESSING_TYPE_THRESHOLD,
        IMAGE_PROCESSING_TYPE_LABELED,
        IMAGE_PROCESSING_TYPE_ERODE
    };
    FeModel();

    void SetInputImage(QImage &);
    inline QImage * GetInputImage()
    {
        return _InputImage;
    }

    void SetSmoothImage(QImage &);
    inline QImage * GetSmoothImage()
    {
        return _SmoothImage;
    }
    void SetThresholdImage(QImage &);
    inline QImage * GetThresholdImage()
    {
        return _ThresholdImage;
    }

    void SetScaledImage(QImage &);
    inline QImage * GetScaledImage()
    {
        return _ScaledImage;
    }
    inline void SetLabeledImage(QImage &img)
    {
        _LabeledImage = &img;
        emit UpdateLabeledImage();
    }
    inline QImage * GetLabeledImage()
    {
        return _LabeledImage;
    }
    inline void SetLegendImage(QImage &img)
    {
        _LegendImage = &img;
        emit UpdateLegendImage();
    }
    inline QImage * GetLegendImage()
    {
        return _LegendImage;
    }
    inline void SetThresHold(int threshold)
    {
        _threshold = threshold;
        // Update threshold image.
        SetSmoothImage(*_SmoothImage);
    }
    inline int GetThreshold() {return _threshold;}
    inline void SetScale(int scale)
    {
        _scale = scale;
        // Update threshold image.
        SetInputImage(*_InputImage);
    }
    inline bool GetInvert() {return invert;}
    inline void SetInvert(bool inv) {invert = inv;}
    inline int GetLabelCount() {return _labelCount;}
    inline void SetLabelCount(int count) {
        _labelCount = count;
        emit UpdateLabelCount();
    }
    inline int GetCircleCount() {return _circleCount;}
    inline void SetCircleCount(int count) {
        _circleCount = count;
        emit UpdateCircleCount();
    }
    void EnableCameraInput(bool enable);

signals:
    void UpdateInputImage();
    void UpdateScaledImage();
    void UpdateSmoothImage();
    void UpdateThresholdImage();
    void UpdateLabeledImage();
    void UpdateLegendImage();
    void UpdateLabelCount();
    void UpdateCircleCount();
private:
    QImage *_InputImage;
    QImage *_ScaledImage;
    QImage *_SmoothImage;
    QImage *_ThresholdImage;
    QImage *_LabeledImage;
    QImage *_LegendImage;

    int _labelCount;
    int _circleCount;

    bool _enableCameraInput;

    FeProcessing* processing;
    int _threshold = 10;
    int _scale = 512;

    bool invert;
};

#endif // EDGEDETECTIONMODEL_H
