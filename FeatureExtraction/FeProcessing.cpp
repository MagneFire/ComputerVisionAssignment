#include "FeProcessing.h"
#include <QFile>
#include <QPainter>
#include <QCameraInfo>
#include <QCameraImageCapture>
#include <QVideoWidget>
#include <QTimer>
QVideoWidget* video;
QImage im;

#include "../Component/CameraFrameGrabber.h"

void FeProcessing::frameUpdated(QImage ime)
{

    im = QImage(ime).convertToFormat(QImage::Format_ARGB32);//video->grab().toImage();
    model.SetInputImage(im);
}
void FeProcessing::EnableCameraInput(bool enable)
{
    if (enable)
    {
        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
        foreach (const QCameraInfo &cameraInfo, cameras) {
            qDebug() << cameraInfo.deviceName();
            qDebug() << cameraInfo.position();
            if ((cameraInfo.deviceName() == "/dev/video1") || (cameraInfo.deviceName() == "front"))
            {
                _camera = new QCamera(cameraInfo);
                _camera->setViewfinder(_camGrabber);
                _camera->start();
                break;
            }
        }
    }
    else
    {
        if (_camera != NULL)
        {
            _camera->stop();
            delete _camera;
        }
    }
}

FeProcessing::FeProcessing(FeModel * model) :
    _camera(NULL),
      model(*model)
{
    _camGrabber = new CameraFrameGrabber();
    connect(_camGrabber, &CameraFrameGrabber::frameAvailable, this, &FeProcessing::frameUpdated);
}

void FeProcessing::DoImageProcessing(FeModel::ImageProcessingType a_ImageProcessingType)
{
    if (model.GetInputImage() == NULL) return;
    _ImageProcessingType = a_ImageProcessingType;
    if (!isRunning())
    {
        start();
    }
}

QImage* CalculateCircularity(QImage *image, QRgb *labels, uint8_t labelCnt, int *circles1)
{
    double array[9];
    // Basically use a structuring element like this:
    //  0   1   0
    //  1   1   1
    //  0   1   0
    // Set default structuring element.
    array[0] = 0.0; array[1] = 1.0; array[2] = 0.0;
    array[3] = 1.0; array[4] = 1.0; array[5] = 1.0;
    array[6] = 1.0; array[7] = 1.0; array[8] = 0.0;

    QImage * erodedImage = NULL;
    QImage * absoluteImage = NULL;

    float sumP;
    float sumA;
    float circularity[labelCnt];

    for (uint8_t i = 0; i < labelCnt; ++i)
    {
        erodedImage = FeUtils::GetMophologyImage(image, FeUtils::IMAGE_PROCESSING_TYPE_EROSION, labels[i], array, 9);
        absoluteImage = FeUtils::GetPImage(image, erodedImage, qGray(labels[i]));
        sumP = 0;
        sumA = 0;
        for (int x = 0; x < image->width(); ++x)
        {
            for (int y = 0; y < image->height(); ++y)
            {
                // The P image contains only the white pixels that define the circumference.
                if (qGray(absoluteImage->pixel(x, y)) == 255)
                {
                    sumP ++;
                }
                // Calculate the total area.
                if (image->pixel(x, y) == labels[i])
                {
                    sumA ++;
                }
            }
        }
        delete absoluteImage;
        delete erodedImage;
        // Calculate the circularity of the labeled object.
        circularity[i] = (4.0 * M_PI * sumA)/qPow(sumP, 2);
    }

    // Get the scaled image.
    QImage* lblImage = new QImage(200, 512, QImage::Format_ARGB32);
    // tell the painter to draw on the QImage
    QPainter* painter = new QPainter(lblImage);
    // Set the pen color to light gray.
    painter->setPen(QColor(220, 220, 220));
    // Set default font size and style.
    painter->setFont(QFont("Arial", 16));
    // Fill a rectangle that is dark gray.
    painter->fillRect(0, 0, 200, 512, QBrush(QColor(0, 0, 0, 255)));

    // Check if the filename might go out of bounce.
    float factor = ((double)lblImage->width()) / painter->fontMetrics().width("Factor: 0.123456");
    if ((factor < 1))
    {
        QFont f = painter->font();
        // Adjust fontsize.
        f.setPointSizeF((int)((double)f.pointSizeF()*(double)factor));
        // Set the new fontsize.
        painter->setFont(f);
    }
    for (uint8_t i = 0; i < labelCnt; ++i)
    {
        // Fill a rectangle that is dark gray.
        painter->fillRect(0, i*25 + 3, 20, 20, QBrush(QColor(labels[i])));
        // Draw the fileame to the image.
        QString shapstr;
        shapstr += "Factor: ";
        shapstr += QString::number(circularity[i]);
        painter->drawText(30, i*25, lblImage->width(), 20, Qt::AlignLeft, shapstr);
    }
    uint64_t circles = 0;
    for (uint8_t i = 0; i < labelCnt; ++i)
    {
        if (circularity[i] > 0.9 && circularity[i] < 1.6)
        {
            circles++;
        }
    }
    // Done painting to the image.
    painter->end();
    *circles1 = circles;
    // Erode image.
    return lblImage;
}

void FeProcessing::run()
{
    // Set new seed for rand().
    qsrand(QTime::currentTime().msec());
    double array[9];
    int total;
    QImage * outputImage;

    QRgb labels[256];
    int labelsCnt = 0;
    int circleCnt = 0;

    FeModel::ImageProcessingType type = _ImageProcessingType;

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    qDebug() << "Starting image processing";
    // Basically use a structuring element like this:
    //  0   1   0
    //  1   1   1
    //  0   1   0
    // Set default structuring element.
    array[0] = 0.0; array[1] = 1.0; array[2] = 0.0;
    array[3] = 1.0; array[4] = 1.0; array[5] = 1.0;
    array[6] = 0.0; array[7] = 1.0; array[8] = 0.0;

    do
    {
        // Update image processing type.
        type = _ImageProcessingType;
        switch(_ImageProcessingType)
        {
        case FeModel::IMAGE_PROCESSING_TYPE_BINARY:
            array[0] = 1.0;
            array[1] = 1.0;
            array[2] = 1.0;
            array[3] = 1.0;
            array[4] = 1.0;
            array[5] = 1.0;
            array[6] = 1.0;
            array[7] = 1.0;
            array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            if (model.GetSmoothImage() != NULL) delete model.GetSmoothImage();
            // Set averaged smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case FeModel::IMAGE_PROCESSING_TYPE_AVERAGE:
            array[0] = 1.0;
            array[1] = 1.0;
            array[2] = 1.0;
            array[3] = 1.0;
            array[4] = 1.0;
            array[5] = 1.0;
            array[6] = 1.0;
            array[7] = 1.0;
            array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            if (model.GetSmoothImage() != NULL) delete model.GetSmoothImage();
            // Set averaged smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case FeModel::IMAGE_PROCESSING_TYPE_GAUSSIAN:
            array[0] = 1.0;
            array[1] = 2.0;
            array[2] = 1.0;
            array[3] = 2.0;
            array[4] = 4.0;
            array[5] = 2.0;
            array[6] = 1.0;
            array[7] = 2.0;
            array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            if (model.GetSmoothImage() != NULL) delete model.GetSmoothImage();
            // Set median smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case FeModel::IMAGE_PROCESSING_TYPE_THRESHOLD:
            // Encode the image.
            outputImage = SmoothingUtils::GetThresholdImage(model.GetSmoothImage(), model.GetInvert(), model.GetThreshold());
            if (model.GetThresholdImage() != NULL) delete model.GetThresholdImage();
            model.SetThresholdImage(*outputImage);
            break;
        case FeModel::IMAGE_PROCESSING_TYPE_LABELED:
            outputImage = FeUtils::GetLabeledImage(model.GetThresholdImage(), labels, 255, &labelsCnt);
            if (model.GetLabeledImage() != NULL) delete model.GetLabeledImage();
            model.SetLabeledImage(*outputImage);
            if (labelsCnt > 0)
            {
               // labelsCnt = FindLabels(outputImage, (QRgb*)&labels, 255);
                model.SetLabelCount(labelsCnt);
                //if (labelsCnt > 2)
                outputImage = CalculateCircularity(outputImage, labels, labelsCnt, &circleCnt);
                model.SetCircleCount(circleCnt);
                if (model.GetLegendImage() != NULL)
                {
                    delete model.GetLegendImage();
                }
                if (outputImage != NULL)
                    model.SetLegendImage(*outputImage);
            }
            break;
        case FeModel::IMAGE_PROCESSING_TYPE_ERODE:
            //outputImage = FeUtils::GetLabeledImage(model.GetThresholdImage());
            //model.SetLabeledImage(*outputImage);
            break;
        }
    } while (type != _ImageProcessingType);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    qDebug() << "Image processed in: "
             << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
             << "us";
}
