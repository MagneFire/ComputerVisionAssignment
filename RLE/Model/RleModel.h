#ifndef RLEMODEL_H
#define RLEMODEL_H

#include <QObject>
class RleProcessing;

class RleModel : public QObject
{
    Q_OBJECT
public:
    enum ImageProcessingType
    {
        IMAGE_PROCESSING_TYPE_BINARY,
        IMAGE_PROCESSING_TYPE_AVERAGE,
        IMAGE_PROCESSING_TYPE_GAUSSIAN,
        IMAGE_PROCESSING_TYPE_THRESHOLD
    };
    RleModel();

    void SetInputImage(QImage &);
    inline QImage * GetInputImage()
    {
        return _InputImage;
    }

    void SetSmoothImage(QImage &);
    inline QImage * GetSmoothImage()
    {
        return _SmoothImage;
    }
    void SetThresholdImage(QImage &);
    inline QImage * GetThresholdImage()
    {
        return _ThresholdImage;
    }

    void SetScaledImage(QImage &);
    inline QImage * GetScaledImage()
    {
        return _ScaledImage;
    }
    inline void SetDiffDilationImage(QImage &img)
    {
        _DiffDilationImage = &img;
        emit UpdateDiffDilationImage();
    }
    inline QImage * GetDiffDilationImage()
    {
        return _DiffDilationImage;
    }
    inline void SetThresHold(int threshold)
    {
        _threshold = threshold;
        // Update threshold image.
        SetSmoothImage(*_SmoothImage);
    }
    inline int GetThreshold() {return _threshold;}
    inline void SetScale(int scale)
    {
        _scale = scale;
        // Update threshold image.
        SetInputImage(*_InputImage);
    }
signals:
    void UpdateInputImage();
    void UpdateScaledImage();
    void UpdateSmoothImage();
    void UpdateThresholdImage();
    void UpdateDiffDilationImage();
private:
    QImage *_InputImage;
    QImage *_ScaledImage;
    QImage *_SmoothImage;
    QImage *_ThresholdImage;
    QImage *_DilationImage;
    QImage *_DiffDilationImage;
    QImage *_ErosionImage;
    QImage *_DiffErosionImage;
    QImage *_OpeningImage;
    QImage *_DiffOpeningImage;
    QImage *_ClosingImage;
    QImage *_DiffClosingImage;
    RleProcessing* processing;
    int _threshold = 10;
    int _scale = 512;
};

#endif // EDGEDETECTIONMODEL_H
