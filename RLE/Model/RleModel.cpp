#include "RleModel.h"
#include "../RleProcessing.h"

RleModel::RleModel():
    _InputImage(NULL),
    _SmoothImage(NULL),
    processing(new RleProcessing(this))
{

}

void RleModel::SetInputImage(QImage & input)
{
    int width = input.width();
    int height = input.height();
    _InputImage = &input;
    emit UpdateInputImage();
    QImage *tmpImage;
    if (width > height) tmpImage = new QImage(input.scaledToWidth(_scale));
    else _ScaledImage = tmpImage = new QImage(input.scaledToHeight(_scale));
    SetScaledImage(*tmpImage);
}

void RleModel::SetScaledImage(QImage & img)
{
    _ScaledImage = &img;
    emit UpdateScaledImage();
    processing->DoImageProcessing(RleModel::IMAGE_PROCESSING_TYPE_BINARY);
}

void RleModel::SetSmoothImage(QImage & img)
{
    _SmoothImage = &img;
    emit UpdateSmoothImage();
    processing->DoImageProcessing(RleModel::IMAGE_PROCESSING_TYPE_THRESHOLD);
}

void RleModel::SetThresholdImage(QImage &img)
{
    _ThresholdImage = &img;
    emit UpdateThresholdImage();
}
