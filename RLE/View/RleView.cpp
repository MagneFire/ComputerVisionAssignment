#include "RleView.h"
#include "ui_RleView.h"
#include <QSlider>
#include <QCheckBox>

RleView::RleView(RleModel & model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RleView),
    model(model)
{
    ui->setupUi(this);


    // Allocate space for new image.
    QImage * tmpImage = new QImage(256, 256, QImage::Format_ARGB32);
    // Make the image transparent.
    tmpImage->fill(qRgba(0, 0, 0, 0));


    ImageList * imageListInput = new ImageList(":/images/");
    connect(imageListInput, &ImageList::ImageClicked, this, &RleView::InputImageClicked);

    ui->ImageList->addWidget(imageListInput);

    _InputImage = new ImageWidget();
    _InputImage->filename = "InputImage.png";
    _InputImage->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    _InputImage->SetEnableInfoDialog(true);
    _InputImage->SetImage(*tmpImage, 64, 64);
    ui->ImageList->insertWidget(0, _InputImage);

    _SmoothImage = new ImageWidget();
    _SmoothImage->filename = "SmoothImage.png";
    _SmoothImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _SmoothImage->SetEnableInfoDialog(true);
    _SmoothImage->SetImage(*tmpImage, 512, 512);
    ui->LayoutH1->addWidget(_SmoothImage);

    _ThresholdImage = new ImageWidget();
    _ThresholdImage->filename = "ThresholdImage.png";
    _ThresholdImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _ThresholdImage->SetEnableInfoDialog(true);
    _ThresholdImage->SetImage(*tmpImage, 512, 512);
    ui->LayoutH1->addWidget(_ThresholdImage);

    connect(&model, &RleModel::UpdateInputImage, this, &RleView::UpdateInputImage, Qt::QueuedConnection);
    connect(&model, &RleModel::UpdateSmoothImage, this, &RleView::UpdateSmoothImage, Qt::QueuedConnection);
    connect(&model, &RleModel::UpdateThresholdImage, this, &RleView::UpdateThresholdImage, Qt::QueuedConnection);

    connect(ui->horizontalSlider, &QSlider::valueChanged, &model, &RleModel::SetThresHold);
    connect(ui->scaleSlider, &QSlider::valueChanged, &model, &RleModel::SetScale);
}

RleView::~RleView()
{
    delete ui;
}


void RleView::InputImageClicked(ImageWidget * object, QMouseEvent * event)
{
    if (event->buttons() == Qt::LeftButton)
    {
        qDebug() << "Performing Image Processing";
        model.SetInputImage(*object->GetImage());
    }
    else if (event->buttons() == Qt::RightButton)
    {
        _ImageInfoDialog.SetTitle(object->filename);
        _ImageInfoDialog.SetImage(*object->GetImage());
        _ImageInfoDialog.adjustSize();
        _ImageInfoDialog.show();
        _ImageInfoDialog.adjustSize();
    }
}
