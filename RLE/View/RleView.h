#ifndef RLEVIEW_H
#define RLEVIEW_H

#include <QWidget>

#include "Component/ImageWidget.h"
#include "Component/ImageList.h"

#include "../Model/RleModel.h"
#include "../../Component/ImageInfoDialog.h"

namespace Ui {
class RleView;
}

class RleView : public QWidget
{
    Q_OBJECT

public:
    explicit RleView(RleModel & model, QWidget *parent = 0);
    ~RleView();

    void InputImageClicked(ImageWidget * object, QMouseEvent * event);

    inline void UpdateInputImage()
    {
        _InputImage->SetImage(*model.GetInputImage(), 64, 64);
    }
    inline void UpdateSmoothImage()
    {
        _SmoothImage->SetImage(*model.GetSmoothImage(), 512, 512);
    }

    inline void UpdateThresholdImage()
    {
        _ThresholdImage->SetImage(*model.GetThresholdImage(), 512, 512);
    }
    inline void ThresholdChanged(int threshold)
    {
        model.SetThresHold(threshold);
    }

private:
    Ui::RleView *ui;
    RleModel & model;
    ImageWidget * _InputImage;
    ImageWidget * _SmoothImage;
    ImageWidget * _ThresholdImage;
    ImageInfoDialog _ImageInfoDialog;
};

#endif // EDGEDETECTIONVIEW_H
