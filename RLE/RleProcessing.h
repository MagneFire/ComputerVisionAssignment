#ifndef RLEPROCESSING_H
#define RLEPROCESSING_H

#include <QDebug>
#include <QObject>
#include <QThread>
#include <QTime>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include "Model/RleModel.h"
#include "../Util/MorphologyUtils.h"
#include "../Util/RleUtils.h"
#include "../Util/SmoothingUtils.h"
#include "../Util/RleUtils.h"
#include "../Util/Utils.h"
//class EdgeDetectionModel;

class RleProcessing : public QThread
{
    Q_OBJECT
public:
    explicit RleProcessing(RleModel * model);
public slots:
    void DoImageProcessing(RleModel::ImageProcessingType);
private:
    /// Thread can only be started using given functions.
    void run();
    RleModel & model;
    RleModel::ImageProcessingType _ImageProcessingType;
};

#endif // EDGEDETECTIONPROCESSING_H
