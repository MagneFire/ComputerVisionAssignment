#include "RleProcessing.h"
#include <QFile>


RleProcessing::RleProcessing(RleModel * model) :
      model(*model)
{

}

void RleProcessing::DoImageProcessing(RleModel::ImageProcessingType a_ImageProcessingType)
{
    if (model.GetInputImage() == NULL) return;
    _ImageProcessingType = a_ImageProcessingType;
    if (!isRunning())
    {
        start();
    }
}

void RleProcessing::run()
{
    // Set new seed for rand().
    qsrand(QTime::currentTime().msec());
    double array[9];
    int total;
    // Calculate the size of the to be allocated space for uint8_t.
    uint32_t binImgSize;
    QImage * outputImage;
    QFile file;
    RleModel::ImageProcessingType type = _ImageProcessingType;

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    qDebug() << "Starting image processing";
    // Basically use a structuring element like this:
    //  0   1   0
    //  1   1   1
    //  0   1   0
    // Set default structuring element.
    array[0] = 0.0; array[1] = 1.0; array[2] = 0.0;
    array[3] = 1.0; array[4] = 1.0; array[5] = 1.0;
    array[6] = 0.0; array[7] = 1.0; array[8] = 0.0;

    do
    {
        // Update image processing type.
        type = _ImageProcessingType;
        switch(_ImageProcessingType)
        {
        case RleModel::IMAGE_PROCESSING_TYPE_BINARY:
            array[0] = 1.0;
            array[1] = 1.0;
            array[2] = 1.0;
            array[3] = 1.0;
            array[4] = 1.0;
            array[5] = 1.0;
            array[6] = 1.0;
            array[7] = 1.0;
            array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set averaged smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case RleModel::IMAGE_PROCESSING_TYPE_AVERAGE:
            array[0] = 1.0;
            array[1] = 1.0;
            array[2] = 1.0;
            array[3] = 1.0;
            array[4] = 1.0;
            array[5] = 1.0;
            array[6] = 1.0;
            array[7] = 1.0;
            array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set averaged smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case RleModel::IMAGE_PROCESSING_TYPE_GAUSSIAN:
            array[0] = 1.0;
            array[1] = 2.0;
            array[2] = 1.0;
            array[3] = 2.0;
            array[4] = 4.0;
            array[5] = 2.0;
            array[6] = 1.0;
            array[7] = 2.0;
            array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set median smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case RleModel::IMAGE_PROCESSING_TYPE_THRESHOLD:
            // Encode the image.
            binImgSize = RleUtils::GetBinaryImage(model.GetSmoothImage(), model.GetThreshold());
            // Check if encoding went successful.
            if (binImgSize)
            {
                file.setFileName("rle-data");
                file.open(QIODevice::WriteOnly);
                // Write RLE encoded image to file.
                file.write((char *)&RleUtils::binaryImage, binImgSize * 4);
                file.close();
                // Decode the RLE encoded image.
                outputImage = RleUtils::GetBinaryImage(
                            (uint32_t*)&RleUtils::binaryImage,
                            model.GetSmoothImage()->width(),
                            model.GetSmoothImage()->height());
                file.setFileName("raw-data");
                file.open(QIODevice::WriteOnly);
                // Write raw image data.
                file.write((const char*)outputImage->constBits(), outputImage->byteCount());
                file.close();
                model.SetThresholdImage(*outputImage);
            }
            break;
        }
    } while (type != _ImageProcessingType);

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    qDebug() << "Image processed in: "
             << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
             << "us";
}
