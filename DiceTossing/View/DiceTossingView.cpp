#include "DiceTossingView.h"
#include "ui_DiceTossingView.h"
#include <QSlider>

DiceTossingView::DiceTossingView(DiceTossingModel & model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DiceTossingView),
    model(model)
{
    ui->setupUi(this);


    // Allocate space for new image.
    QImage * tmpImage = new QImage(256, 256, QImage::Format_ARGB32);
    // Make the image transparent.
    tmpImage->fill(qRgba(0, 0, 0, 0));


    ImageList * imageListInput = new ImageList(":/images/");
    connect(imageListInput, &ImageList::ImageClicked, this, &DiceTossingView::InputImageClicked);

    ui->ImageList->addWidget(imageListInput);

    _InputImage = new ImageWidget();
    _InputImage->filename = "InputImage.png";
    _InputImage->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    _InputImage->SetEnableInfoDialog(true);
    _InputImage->SetImage(*tmpImage, 64, 64);
    ui->ImageList->insertWidget(0, _InputImage);

    _SmoothImage = new ImageWidget();
    _SmoothImage->filename = "SmoothImage.png";
    _SmoothImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _SmoothImage->SetEnableInfoDialog(true);
    _SmoothImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH1->addWidget(_SmoothImage);

    _SXImage = new ImageWidget();
    _SXImage->filename = "SXImage.png";
    _SXImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _SXImage->SetEnableInfoDialog(true);
    _SXImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH2->addWidget(_SXImage);

    _SYImage = new ImageWidget();
    _SYImage->filename = "SYImage.png";
    _SYImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _SYImage->SetEnableInfoDialog(true);
    _SYImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH2->addWidget(_SYImage);

    _GradientImage = new ImageWidget();
    _GradientImage->filename = "GradientImage.png";
    _GradientImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _GradientImage->SetEnableInfoDialog(true);
    _GradientImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH1->addWidget(_GradientImage);

    _ThresholdImage = new ImageWidget();
    _ThresholdImage->filename = "ThresholdImage.png";
    _ThresholdImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _ThresholdImage->SetEnableInfoDialog(true);
    _ThresholdImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutH1->addWidget(_ThresholdImage);

    connect(&model, &DiceTossingModel::UpdateInputImage, this, &DiceTossingView::UpdateInputImage, Qt::QueuedConnection);
    connect(&model, &DiceTossingModel::UpdateSmoothImage, this, &DiceTossingView::UpdateSmoothImage, Qt::QueuedConnection);
    connect(&model, &DiceTossingModel::UpdateSXImage, this, &DiceTossingView::UpdateSXImage, Qt::QueuedConnection);
    connect(&model, &DiceTossingModel::UpdateSYImage, this, &DiceTossingView::UpdateSYImage, Qt::QueuedConnection);
    connect(&model, &DiceTossingModel::UpdateGradientImage, this, &DiceTossingView::UpdateGradientImage, Qt::QueuedConnection);
    connect(&model, &DiceTossingModel::UpdateThresholdImage, this, &DiceTossingView::UpdateThresholdImage, Qt::QueuedConnection);
    connect(ui->horizontalSlider, &QSlider::valueChanged, &model, &DiceTossingModel::SetThresHold);
    connect(ui->scaleSlider, &QSlider::valueChanged, &model, &DiceTossingModel::SetScale);
}

DiceTossingView::~DiceTossingView()
{
    delete ui;
}


void DiceTossingView::InputImageClicked(ImageWidget * object, QMouseEvent * event)
{
    if (event->buttons() == Qt::LeftButton)
    {
        qDebug() << "Performing Image Processing";
        model.SetInputImage(*object->GetImage());
    }
    else if (event->buttons() == Qt::RightButton)
    {
        _ImageInfoDialog.SetTitle(object->filename);
        _ImageInfoDialog.SetImage(*object->GetImage());
        _ImageInfoDialog.adjustSize();
        _ImageInfoDialog.show();
        _ImageInfoDialog.adjustSize();
    }
}
