#ifndef DICETOSSINGVIEW_H
#define DICETOSSINGVIEW_H

#include <QWidget>

#include "Component/ImageWidget.h"
#include "Component/ImageList.h"

#include "../Model/DiceTossingModel.h"
#include "../../Component/ImageInfoDialog.h"

namespace Ui {
class DiceTossingView;
}

class DiceTossingView : public QWidget
{
    Q_OBJECT

public:
    explicit DiceTossingView(DiceTossingModel & model, QWidget *parent = 0);
    ~DiceTossingView();

    void InputImageClicked(ImageWidget * object, QMouseEvent * event);

    inline void UpdateInputImage()
    {
        _InputImage->SetImage(*model.GetInputImage(), 64, 64);
    }
    inline void UpdateSmoothImage()
    {
        _SmoothImage->SetImage(*model.GetSmoothImage(), 256, 256);
    }
    inline void UpdateSXImage()
    {
        _SXImage->SetImage(*model.GetSXImage(), 256, 256);
    }
    inline void UpdateSYImage()
    {
        _SYImage->SetImage(*model.GetSYImage(), 256, 256);
    }
    inline void UpdateGradientImage()
    {
        _GradientImage->SetImage(*model.GetGradientImage(), 256, 256);
    }
    inline void UpdateThresholdImage()
    {
        _ThresholdImage->SetImage(*model.GetThresholdImage(), 256, 256);
    }
    inline void ThresholdChanged(int threshold)
    {
        model.SetThresHold(threshold);
    }

private:
    Ui::DiceTossingView *ui;
    DiceTossingModel & model;
    ImageWidget * _InputImage;
    ImageWidget * _SmoothImage;
    ImageWidget * _SXImage;
    ImageWidget * _SYImage;
    ImageWidget * _GradientImage;
    ImageWidget * _ThresholdImage;
    ImageInfoDialog _ImageInfoDialog;
};

#endif // DiceTossingVIEW_H
