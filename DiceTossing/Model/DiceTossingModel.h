#ifndef DICETOSSINGMODEL_H
#define DICETOSSINGMODEL_H

#include <QObject>
class DiceTossingProcessing;

class DiceTossingModel : public QObject
{
    Q_OBJECT
public:
    enum ImageProcessingType
    {
        IMAGE_PROCESSING_TYPE_AVERAGE,
        IMAGE_PROCESSING_TYPE_GAUSSIAN,
        IMAGE_PROCESSING_TYPE_SX,
        IMAGE_PROCESSING_TYPE_SY,
        IMAGE_PROCESSING_TYPE_GRADIENT,
        IMAGE_PROCESSING_TYPE_THRESHOLD
    };
    DiceTossingModel();

    void SetInputImage(QImage &);
    inline QImage * GetInputImage()
    {
        return _InputImage;
    }

    void SetSmoothImage(QImage &);
    inline QImage * GetSmoothImage()
    {
        return _SmoothImage;
    }

    void SetSXImage(QImage &);
    inline QImage * GetSXImage()
    {
        return _SXImage;
    }
    void SetSYImage(QImage &);
    inline QImage * GetSYImage()
    {
        return _SYImage;
    }
    void SetGradientImage(QImage &);
    inline QImage * GetGradientImage()
    {
        return _GradientImage;
    }
    void SetThresholdImage(QImage &);
    inline QImage * GetThresholdImage()
    {
        return _ThresholdImage;
    }

    void SetScaledImage(QImage &);
    inline QImage * GetScaledImage()
    {
        return _ScaledImage;
    }
    inline void SetThresHold(int threshold)
    {
        _threshold = threshold;
        // Update threshold image.
        SetGradientImage(*_GradientImage);
    }
    inline int GetThreshold() {return _threshold;}
    inline void SetScale(int scale)
    {
        _scale = scale;
        // Update threshold image.
        SetInputImage(*_InputImage);
    }

signals:
    void UpdateInputImage();
    void UpdateScaledImage();
    void UpdateSmoothImage();
    void UpdateSXImage();
    void UpdateSYImage();
    void UpdateGradientImage();
    void UpdateThresholdImage();
private:
    QImage *_InputImage;
    QImage *_ScaledImage;
    QImage *_SmoothImage;
    QImage *_SXImage;
    QImage *_SYImage;
    QImage *_GradientImage;
    QImage *_ThresholdImage;
    DiceTossingProcessing* processing;
    int _threshold = 10;
    int _scale = 512;
};

#endif // DiceTossingMODEL_H
