#include "DiceTossingModel.h"
#include "../DiceTossingProcessing.h"

DiceTossingModel::DiceTossingModel():
    _InputImage(NULL),
    _SmoothImage(NULL),
    _SXImage(NULL),
    _SYImage(NULL),
    processing(new DiceTossingProcessing(this))
{

}

void DiceTossingModel::SetInputImage(QImage & input)
{
    int width = input.width();
    int height = input.height();
    _InputImage = &input;
    emit UpdateInputImage();
    QImage *tmpImage;
    if (width > height) tmpImage = new QImage(input.scaledToWidth(_scale));
    else _ScaledImage = tmpImage = new QImage(input.scaledToHeight(_scale));
    SetScaledImage(*tmpImage);
}

void DiceTossingModel::SetScaledImage(QImage & img)
{
    _ScaledImage = &img;
    emit UpdateScaledImage();
    processing->DoImageProcessing(DiceTossingModel::IMAGE_PROCESSING_TYPE_AVERAGE);
}

void DiceTossingModel::SetSmoothImage(QImage & img)
{
    _SmoothImage = &img;
    emit UpdateSmoothImage();
    processing->DoImageProcessing(DiceTossingModel::IMAGE_PROCESSING_TYPE_SX);
}

void DiceTossingModel::SetSXImage(QImage & img)
{
    _SXImage = &img;
    emit UpdateSXImage();
    processing->DoImageProcessing(DiceTossingModel::IMAGE_PROCESSING_TYPE_SY);
}
void DiceTossingModel::SetSYImage(QImage & img)
{
    _SYImage = &img;
    emit UpdateSYImage();
    processing->DoImageProcessing(DiceTossingModel::IMAGE_PROCESSING_TYPE_GRADIENT);
}

void DiceTossingModel::SetGradientImage(QImage &img)
{
    _GradientImage = &img;
    emit UpdateGradientImage();
    processing->DoImageProcessing(DiceTossingModel::IMAGE_PROCESSING_TYPE_THRESHOLD);
}

void DiceTossingModel::SetThresholdImage(QImage &img)
{
    _ThresholdImage = &img;
    emit UpdateThresholdImage();
}
