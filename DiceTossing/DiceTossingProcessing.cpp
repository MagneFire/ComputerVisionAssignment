#include "DiceTossingProcessing.h"

#include <opencv/cv.h>
#include <opencv/cxcore.h>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <QTimer>

using namespace cv;

// The background frame, taken at program startup.
Mat background;

// The input frame from the USB camera.
// This will also be used for overlaying the detected dice and its corresponding amount of pips.
Mat frame;
// The frame that will be used for detecting the pips on the dice.
Mat processFrame;

VideoCapture videoCapture(1);

int CountPipsOnDie(Mat die)
{
    // Resize the dice area in order to make it easier to change the corner areas.
    resize(die, die, Size(150, 150));

    // Thresholding.
    threshold(die, die, 150, 255, THRESH_BINARY | CV_THRESH_OTSU );

    // Remove black area between outer edge and outer side of the image.
    floodFill(die, Point(0,0), Scalar(255));
    floodFill(die, Point(0,149), Scalar(255));
    floodFill(die, Point(149,0), Scalar(255));
    floodFill(die, Point(149,149), Scalar(255));

    // Now remove white area from the outer edge to the inner edge.
    // This leaves only the pips edges.
    floodFill(die, Point(0,0), Scalar(0));

    // Remove the edge detection borders.
    // This leaves only the actual pips.
    floodFill(die, Point(0,0), Scalar(255));

    // Use the blob detector to count the black blobs(the pips).
    SimpleBlobDetector::Params param;

    param.filterByArea = true;
    param.filterByCircularity = false;
    param.filterByConvexity = false;
    param.filterByInertia = false;
    param.filterByColor = true;
    param.blobColor = 0;
    param.maxArea = 10000;

    std::vector<KeyPoint> pipPoints;
    Ptr<SimpleBlobDetector> pipDetector = SimpleBlobDetector::create(param);

    // Count pips.
    pipDetector->detect(die, pipPoints);
    if (pipPoints.size() > 0)
      imshow("dice", die);

    // Return pip count.
    return pipPoints.size();
}

void DiceTossingProcessing::Timeout()
{
    // Get a frame.
    videoCapture >> frame;
    cvtColor(frame, frame, CV_BGR2GRAY);
    imshow("frame", frame);
    // Get a copy to process.
    processFrame = frame.clone();

    // Remove the background.
    absdiff(processFrame, background, processFrame);
    // Remove some noise by applying a blur.
    GaussianBlur( processFrame, processFrame, Size(3, 3), 2, 2 );
    // Apply thresholding.
    threshold(processFrame, processFrame, 150, 255, THRESH_BINARY | CV_THRESH_OTSU );
    // Apply edge detection.
    Canny( processFrame, processFrame, 6, 12);

    Mat element = getStructuringElement( MORPH_CROSS,
                                         Size( 2*1 + 1, 2*1+1 ),
                                         Point( 1, 1 ) );

    // Apply the dilation operation.
    dilate(processFrame, processFrame, element );

    imshow("process", processFrame);

    std::vector<std::vector<Point>> diceContours;
    std::vector<Vec4i> diceHierarchy;
    // Find contours, only find the extreme outer contours.
    findContours( processFrame.clone(), diceContours, diceHierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE );

    // Limit the amount of dice that may be detected.
    if (diceContours.size() > 60) return;

    std::vector<std::vector<Point> > drawnContours;
    cvtColor(frame, frame, CV_GRAY2BGR);
    for(size_t i = 0; i < diceContours.size(); i++)
    {
        // get bounding rect
        Rect diceBoundsRect = boundingRect(Mat(diceContours[i]));

        // Check if the current contour points provided, are whitin an already processed image.
        // There is no need to reprocess this again, plus reprocessing will add more redundant overlay information.
        bool alreadyProcessed = false;
        std::vector<std::vector<Point> >::iterator it;
        for(it=drawnContours.begin() ; it < drawnContours.end(); it++)
        {
            std::vector<Point>::iterator it1;
            for(it1=(*it).begin() ; it1 < (*it).end(); it1++)
            {
                if (diceBoundsRect.contains(*it1))
                {
                    alreadyProcessed = true;
                    break;
                }
            }
        }
        if (alreadyProcessed) continue;

        // Get the surrounding area where a die has been detected.
        Mat dieArea = processFrame(diceBoundsRect);
        int pipCount = CountPipsOnDie(dieArea);

        if(pipCount > 0 && pipCount <= 6)
        {
            drawnContours.push_back(diceContours[i]);

            std::string die("pips: " + std::to_string(pipCount));

            // Write the amount of pips counted to the screen.
            putText(frame, die, Point(diceBoundsRect.x, diceBoundsRect.y + diceBoundsRect.height + 20 ), FONT_HERSHEY_COMPLEX_SMALL, 0.8, Scalar::all(255));

            // Write the edge detected pixels over the output image.
            for(int i = 0; i < processFrame.rows; i++)
            {
                for(int j = 0; j < processFrame.cols; j++)
                {
                    uchar pixel = processFrame.at<uchar>(i, j);
                    if (pixel > 0)
                    {
                        frame.at<Vec3b>(i, j)[0] = 0;
                        frame.at<Vec3b>(i, j)[1] = 0;
                        frame.at<Vec3b>(i, j)[2] = 255;
                    }
                }
            }
        }
    }
    imshow("processed", frame);
}

DiceTossingProcessing::DiceTossingProcessing(DiceTossingModel * model) :
      model(*model)
{
    if(videoCapture.isOpened())
    {
        // Get a frame and use this as the background image.
        videoCapture >> background;
        // Convert background image to grayscale.
        cvtColor(background, background, CV_BGR2GRAY);

        imshow("background", background);

        timer.setInterval(400);
        connect(&timer, &QTimer::timeout, this, &DiceTossingProcessing::Timeout);
        timer.start();
    }

}

void DiceTossingProcessing::DoImageProcessing(DiceTossingModel::ImageProcessingType a_ImageProcessingType)
{
    if (model.GetInputImage() == NULL) return;
    _ImageProcessingType = a_ImageProcessingType;
    if (!isRunning())
    {
        start();
    }
}

void DiceTossingProcessing::run()
{
    // Set new seed for rand().
    qsrand(QTime::currentTime().msec());
    double array[9];
    int total;
    DiceTossingModel::ImageProcessingType type = _ImageProcessingType;

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    qDebug() << "Starting image processing";

    do
    {
        // Update image processing type.
        type = _ImageProcessingType;
        switch(_ImageProcessingType)
        {
        case DiceTossingModel::IMAGE_PROCESSING_TYPE_AVERAGE:
            // Set the average filter.
            array[0] = 1.0; array[1] = 1.0; array[2] = 1.0;
            array[3] = 1.0; array[4] = 1.0; array[5] = 1.0;
            array[6] = 1.0; array[7] = 1.0; array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set averaged smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case DiceTossingModel::IMAGE_PROCESSING_TYPE_GAUSSIAN:
            // Set the Gaussian filter.
            array[0] = 1.0; array[1] = 2.0; array[2] = 1.0;
            array[3] = 2.0; array[4] = 4.0; array[5] = 2.0;
            array[6] = 1.0; array[7] = 2.0; array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set median smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case DiceTossingModel::IMAGE_PROCESSING_TYPE_SX:
            // Set Sobel X operator/filter.
            array[0] = -1.0; array[1] = -2.0; array[2] = -1.0;
            array[3] =  0.0; array[4] =  0.0; array[5] =  0.0;
            array[6] =  1.0; array[7] =  2.0; array[8] =  1.0;
            // Set Sobel X image.
            model.SetSXImage(*SmoothingUtils::GetFilteredImage(model.GetSmoothImage(), array, 9));
            break;
        case DiceTossingModel::IMAGE_PROCESSING_TYPE_SY:
            // Set Sobel Y operator/filter.
            array[0] = -1.0; array[1] =  0.0; array[2] =  1.0;
            array[3] = -2.0; array[4] =  0.0; array[5] =  2.0;
            array[6] = -1.0; array[7] =  0.0; array[8] =  1.0;
            // Set Sobel Y image.
            model.SetSYImage(*SmoothingUtils::GetFilteredImage(model.GetSmoothImage(), array, 9));
            break;
        case DiceTossingModel::IMAGE_PROCESSING_TYPE_GRADIENT:
            // Set gradient image.
            model.SetGradientImage(*SmoothingUtils::GetGradientImage(model.GetSXImage(), model.GetSYImage()));
            break;
        case DiceTossingModel::IMAGE_PROCESSING_TYPE_THRESHOLD:
            model.SetThresholdImage(*SmoothingUtils::GetThresholdImage(model.GetGradientImage(), false, model.GetThreshold()));
        }
    } while (type != _ImageProcessingType);


    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    qDebug() << "Image processed in: "
             << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
             << "us";
}
