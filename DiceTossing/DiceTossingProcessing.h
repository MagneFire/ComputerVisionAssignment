#ifndef DICETOSSINGPROCESSING_H
#define DICETOSSINGPROCESSING_H

#include <QDebug>
#include <QObject>
#include <QThread>
#include <QTime>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include "Model/DiceTossingModel.h"
#include "../Util/SmoothingUtils.h"
#include "../Util/Utils.h"

#include <QTimer>

class DiceTossingProcessing : public QThread
{
    Q_OBJECT
private:
    QTimer timer;
public:
    explicit DiceTossingProcessing(DiceTossingModel * model);
public slots:
    void DoImageProcessing(DiceTossingModel::ImageProcessingType);
    void Timeout();
private:
    /// Thread can only be started using given functions.
    void run();
    DiceTossingModel & model;
    DiceTossingModel::ImageProcessingType _ImageProcessingType;
};

#endif // DiceTossingPROCESSING_H
