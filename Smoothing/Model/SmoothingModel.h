#ifndef SMOOTHINGMODEL_H
#define SMOOTHINGMODEL_H

#include <QObject>
#include <QImage>

class SmoothingModel : public QObject
{
    Q_OBJECT
public:

    enum ImageProcessingType
    {
        IMAGE_PROCESSING_TYPE_UNIFORM,
        IMAGE_PROCESSING_TYPE_GAUSSIAN,
        IMAGE_PROCESSING_TYPE_SALTNPEPPER,
        IMAGE_PROCESSING_TYPE_AVERAGE,
        IMAGE_PROCESSING_TYPE_MEDIAN
    };

    SmoothingModel();

    void SetManipulatedImage(QImage &);
    QImage * GetManipulatedImage();

    void SetInputImage(QImage &);
    QImage * GetInputImage();

    void SetAveragedImage(QImage &);
    QImage * GetAveragedImage();

    void SetAbsoluteAveragedImage(QImage &);
    QImage * GetAbsoluteAveragedImage();

    void SetAbsoluteMedianImage(QImage &);
    QImage * GetAbsoluteMedianImage();

    void SetMedianImage(QImage &);
    QImage * GetMedianImage();

    void SetResolution(uint8_t);
    uint8_t GetResolution();

    void SetUniformMagnitude(double magnitude);
    double GetUniformMagnitude();

    void SetGaussianDeviation(int deviation);
    int GetGaussianDeviation();

    void SetSaltNPepperDensity(double density);
    double GetSaltNPepperDensity();
signals:
    void UpdateAverageImage();
    void UpdateAbsoluteAverageImage();
    void UpdateMedianImage();
    void UpdateAbsoluteMedianImage();
    void UpdateInputImage();
    void UpdateManipulatedImage();
    void DoImageProcessing(SmoothingModel::ImageProcessingType);
private:
    QImage *_ManipulatedImage;

    QImage *_InputImage;
    QImage *_AveragedImage;
    QImage *_AbsoluteAveragedImage;
    QImage *_MedianImage;
    QImage *_AbsoluteMedianImage;

    uint8_t _resolution;

    double _UniformMagnitude;
    int _GaussianDeviation;
    double _SaltNPepperDensity;
};

#endif // SMOOTHINGMODEL_H
