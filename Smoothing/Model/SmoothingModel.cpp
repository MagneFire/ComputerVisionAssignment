#include "SmoothingModel.h"

#include <QDebug>

SmoothingModel::SmoothingModel():
    _ManipulatedImage(NULL),
    _InputImage(NULL),
    _AveragedImage(NULL),
    _AbsoluteAveragedImage(NULL),
    _MedianImage(NULL),
    _AbsoluteMedianImage(NULL),
    _resolution(3)
{

}


void SmoothingModel::SetManipulatedImage(QImage & a_Image)
{
    _ManipulatedImage = &a_Image;
    emit UpdateManipulatedImage();
}

QImage * SmoothingModel::GetManipulatedImage()
{
    return _ManipulatedImage;
}

void SmoothingModel::SetInputImage(QImage & a_Image)
{
    _InputImage = &a_Image;
    emit UpdateInputImage();
}

QImage * SmoothingModel::GetInputImage()
{
    return _InputImage;
}

void SmoothingModel::SetAveragedImage(QImage & a_Image)
{
    _AveragedImage = &a_Image;
    emit UpdateAverageImage();
}

QImage * SmoothingModel::GetAveragedImage()
{
    return _AveragedImage;
}

void SmoothingModel::SetMedianImage(QImage & a_Image)
{
    _MedianImage = &a_Image;
    emit UpdateMedianImage();
}

QImage * SmoothingModel::GetMedianImage()
{
    return _MedianImage;
}

void SmoothingModel::SetAbsoluteAveragedImage(QImage & a_Image)
{
    _AbsoluteAveragedImage = &a_Image;
    emit UpdateAbsoluteAverageImage();
}

QImage * SmoothingModel::GetAbsoluteAveragedImage()
{
    return _AbsoluteAveragedImage;
}

void SmoothingModel::SetAbsoluteMedianImage(QImage & a_Image)
{
    _AbsoluteMedianImage = &a_Image;
    emit UpdateAbsoluteMedianImage();
}

QImage * SmoothingModel::GetAbsoluteMedianImage()
{
    return _AbsoluteMedianImage;
}

void SmoothingModel::SetResolution(uint8_t resolution)
{
    _resolution = resolution;
}

uint8_t SmoothingModel::GetResolution()
{
    return _resolution;
}

void SmoothingModel::SetUniformMagnitude(double magnitude)
{
    _UniformMagnitude = magnitude;
}

double SmoothingModel::GetUniformMagnitude()
{
    return _UniformMagnitude;
}

void SmoothingModel::SetGaussianDeviation(int deviation)
{
    _GaussianDeviation = deviation;
}

int SmoothingModel::GetGaussianDeviation()
{
    return _GaussianDeviation;
}

void SmoothingModel::SetSaltNPepperDensity(double density)
{
    _SaltNPepperDensity = density;
}

double SmoothingModel::GetSaltNPepperDensity()
{
    return _SaltNPepperDensity;
}
