#include "SmoothingView.h"
#include "ui_SmoothingView.h"

#include "Component/ImageWidget.h"
#include "Component/ImageList.h"

#include <QDirIterator>

#include <QDebug>
#include <QDesktopWidget>

ImageWidget * _AverageImage;
ImageWidget * _MedianImage;
ImageWidget * _AbsoluteAverageImage;
ImageWidget * _AbsoluteMedianImage;

ImageWidget * _InputImage;

ImageWidget * _ManipulatedImage;

SmoothingView::SmoothingView(SmoothingModel & a_SmoothingModel, SmoothingController & a_SmoothingController) :
    QMainWindow(0),
    ui(new Ui::SmoothingView),
    _SmoothingModel(a_SmoothingModel)
{
    ui->setupUi(this);

    // Allocate space for new image.
    QImage * tmpImage = new QImage(256, 256, QImage::Format_ARGB32);
    // Make the image transparent.
    tmpImage->fill(qRgba(0, 0, 0, 0));

    ImageList * imageListInput = new ImageList(":/images/");
    connect(imageListInput, SIGNAL(ImageClicked(ImageWidget*,QMouseEvent*)), &a_SmoothingController, SLOT(InputImageClicked(ImageWidget*,QMouseEvent*)));

    ui->ImageList->addWidget(imageListInput);

    connect(ui->AverageButton, SIGNAL(clicked(bool)), this, SLOT(AverageButtonClicked()));
    connect(ui->MedianButton, SIGNAL(clicked(bool)), this, SLOT(MedianButtonClicked()));

    connect(ui->ResolutionSlider, SIGNAL(valueChanged(int)), this, SLOT(ResolutionSliderChanged(int)));

    connect(ui->UniformApplybtn, SIGNAL(clicked(bool)), this, SLOT(UniformApplyButtonClicked()));
    connect(ui->GausianApplybtn, SIGNAL(clicked(bool)), this, SLOT(GaussianApplyButtonClicked()));
    connect(ui->SaltNPepperApplybtn, SIGNAL(clicked(bool)), this, SLOT(SaltNPepperApplyButtonClicked()));

    _InputImage = new ImageWidget();
    _InputImage->filename = "InputImage.png";
    _InputImage->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    _InputImage->SetEnableInfoDialog(true);
    _InputImage->SetImage(*tmpImage, 64, 64);
    ui->ImageList->insertWidget(0, _InputImage);


    _ManipulatedImage = new ImageWidget();
    _ManipulatedImage->filename = "ManipulatedImage.png";
    _ManipulatedImage->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    _ManipulatedImage->SetEnableInfoDialog(true);
    _ManipulatedImage->SetImage(*tmpImage, 256, 256);
    ui->ImagePropertiesLayout->addWidget(_ManipulatedImage);

    _AverageImage = new ImageWidget();
    _AverageImage->filename = "AverageImage.png";
    _AverageImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _AverageImage->SetEnableInfoDialog(true);
    _AverageImage->SetImage(*tmpImage, 256, 256);
    ui->horizontalLayout_2->addWidget(_AverageImage);

    _MedianImage = new ImageWidget();
    _MedianImage->filename = "MedianImage.png";
    _MedianImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _MedianImage->SetEnableInfoDialog(true);
    _MedianImage->SetImage(*tmpImage, 256, 256);
    ui->horizontalLayout_2->addWidget(_MedianImage);

    _AbsoluteAverageImage = new ImageWidget();
    _AbsoluteAverageImage->filename = "AbsoluteAverageImage.png";
    _AbsoluteAverageImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _AbsoluteAverageImage->SetEnableInfoDialog(true);
    _AbsoluteAverageImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutAbsolute->addWidget(_AbsoluteAverageImage);

    _AbsoluteMedianImage = new ImageWidget();
    _AbsoluteMedianImage->filename = "AbsoluteMedianImage.png";
    _AbsoluteMedianImage->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    _AbsoluteMedianImage->SetEnableInfoDialog(true);
    _AbsoluteMedianImage->SetImage(*tmpImage, 256, 256);
    ui->LayoutAbsolute->addWidget(_AbsoluteMedianImage);

    // Make magnitude LineEdit for doubles only.
    ui->UniformMagnetudele->setValidator(new QDoubleValidator(0, 100, 2, this));

    // Make deviation LineEdit for doubles only.
    ui->GaussianDeviationle->setValidator(new QDoubleValidator(0, 100, 2, this));

    // Center the window on the screen.
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

SmoothingView::~SmoothingView()
{
    delete ui;
}

void SmoothingView::UniformApplyButtonClicked()
{
    _SmoothingModel.SetUniformMagnitude(ui->UniformMagnetudele->text().toDouble());
    _SmoothingModel.DoImageProcessing(SmoothingModel::IMAGE_PROCESSING_TYPE_UNIFORM);
}

void SmoothingView::GaussianApplyButtonClicked()
{
    _SmoothingModel.SetGaussianDeviation(ui->GaussianDeviationle->text().toInt());
    _SmoothingModel.DoImageProcessing(SmoothingModel::IMAGE_PROCESSING_TYPE_GAUSSIAN);
}

void SmoothingView::SaltNPepperApplyButtonClicked()
{
    _SmoothingModel.SetSaltNPepperDensity(ui->SaltNPepperDensityle->text().toDouble());
    _SmoothingModel.DoImageProcessing(SmoothingModel::IMAGE_PROCESSING_TYPE_SALTNPEPPER);
}

void SmoothingView::AverageButtonClicked()
{
    _SmoothingModel.SetResolution(ui->ResolutionSlider->value()*2+1);
    _SmoothingModel.DoImageProcessing(SmoothingModel::IMAGE_PROCESSING_TYPE_AVERAGE);
}

void SmoothingView::MedianButtonClicked()
{
    _SmoothingModel.SetResolution(ui->ResolutionSlider->value()*2+1);
    _SmoothingModel.DoImageProcessing(SmoothingModel::IMAGE_PROCESSING_TYPE_MEDIAN);
}

void SmoothingView::ResolutionSliderChanged(int)
{
    ui->ResolutionLabel->setText(QString::number(ui->ResolutionSlider->value()*2+1) + "x" + QString::number(ui->ResolutionSlider->value()*2+1));
}

void SmoothingView::UpdateInputImage()
{
    _InputImage->SetImage(*_SmoothingModel.GetInputImage(), 64, 64);
}

void SmoothingView::UpdateAverageImage()
{
    _AverageImage->SetImage(*_SmoothingModel.GetAveragedImage(), 256, 256);
}

void SmoothingView::UpdateMedianImage()
{
    _MedianImage->SetImage(*_SmoothingModel.GetMedianImage(), 256, 256);
}
void SmoothingView::UpdateAbsoluteAverageImage()
{
    _AbsoluteAverageImage->SetImage(*_SmoothingModel.GetAbsoluteAveragedImage(), 256, 256);
}

void SmoothingView::UpdateAbsoluteMedianImage()
{
    _AbsoluteMedianImage->SetImage(*_SmoothingModel.GetAbsoluteMedianImage(), 256, 256);
}

void SmoothingView::UpdateManipulatedImage()
{
    _ManipulatedImage->SetImage(*_SmoothingModel.GetManipulatedImage(), 256, 256);
}
