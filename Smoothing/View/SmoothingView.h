#ifndef SMOOTHINGVIEW_H
#define SMOOTHINGVIEW_H

#include <QMainWindow>

#include "Smoothing/Model/SmoothingModel.h"
#include "Smoothing/Controller/SmoothingController.h"

namespace Ui {
class SmoothingView;
}

class SmoothingView : public QMainWindow
{
    Q_OBJECT

public:
    explicit SmoothingView(SmoothingModel &, SmoothingController &);
    ~SmoothingView();
public slots:
    void UpdateAverageImage();
    void UpdateMedianImage();
    void UpdateAbsoluteAverageImage();
    void UpdateAbsoluteMedianImage();
    void UpdateInputImage();
    void UpdateManipulatedImage();

    void AverageButtonClicked();
    void MedianButtonClicked();
    void ResolutionSliderChanged(int);

    void UniformApplyButtonClicked();
    void GaussianApplyButtonClicked();
    void SaltNPepperApplyButtonClicked();
private:
    Ui::SmoothingView *ui;
    SmoothingModel & _SmoothingModel;
};

#endif // SMOOTHINGVIEW_H
