#ifndef SMOOTHINGPROCESSING_H
#define SMOOTHINGPROCESSING_H

#include <QObject>
#include <QThread>
#include <QTime>

#include "Smoothing/Model/SmoothingModel.h"

#include "Util/SmoothingUtils.h"

class SmoothingProcessing : public QThread
{
    Q_OBJECT
public:
    explicit SmoothingProcessing(SmoothingModel &);
signals:

public slots:
    void DoImageProcessing(SmoothingModel::ImageProcessingType);
private:
    /// Thread can only be started using given functions.
    void run();
    SmoothingModel & _SmoothingModel;
    SmoothingModel::ImageProcessingType _ImageProcessingType;
};

#endif // SMOOTHINGPROCESSING_H
