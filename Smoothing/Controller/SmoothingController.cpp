#include "SmoothingController.h"
#include <QDebug>



SmoothingController::SmoothingController(SmoothingModel & a_SmoothingModel) :
    _SmoothingModel(a_SmoothingModel)
{

}

void SmoothingController::InputImageClicked(ImageWidget * object, QMouseEvent * event)
{
    if (event->buttons() == Qt::LeftButton)
    {
        qDebug() << "Performing Image Processing";
        _SmoothingModel.SetInputImage(*object->GetImage());
        _SmoothingModel.SetManipulatedImage(*object->GetImage());
    }
    else if (event->buttons() == Qt::RightButton)
    {
        _ImageInfoDialog.SetTitle(object->filename);
        _ImageInfoDialog.SetImage(*object->GetImage());
        _ImageInfoDialog.adjustSize();
        _ImageInfoDialog.show();
        _ImageInfoDialog.adjustSize();
    }
}

void SmoothingController::InputImageClicked(QObject * object, QMouseEvent * event)
{
    InputImageClicked(static_cast<ImageWidget*>(object), event);
}
