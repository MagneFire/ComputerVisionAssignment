#ifndef SMOOTHINGCONTROLLER_H
#define SMOOTHINGCONTROLLER_H

#include <QObject>
#include <QMouseEvent>

#include "Component/ImageInfoDialog.h"

#include "Smoothing/Model/SmoothingModel.h"
#include "Component/ImageWidget.h"

class SmoothingController : public QObject
{
    Q_OBJECT
public:
    SmoothingController(SmoothingModel &);

private:
    SmoothingModel & _SmoothingModel;
public slots:
    void InputImageClicked(QObject *, QMouseEvent *);
    void InputImageClicked(ImageWidget *, QMouseEvent *);
private:
    ImageInfoDialog _ImageInfoDialog;
};

#endif // HISTOGRAMCONTROLLER_H
