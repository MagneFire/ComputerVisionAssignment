#include "SmoothingProcessing.h"

#include "Util/ImageUtils.h"
#include "Util/NoiseUtils.h"
#include "Util/Utils.h"

#include <QDebug>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

SmoothingProcessing::SmoothingProcessing(SmoothingModel & a_SmoothingModel) :
    _SmoothingModel(a_SmoothingModel)
{
}

void SmoothingProcessing::DoImageProcessing(SmoothingModel::ImageProcessingType a_ImageProcessingType)
{
    if (_SmoothingModel.GetInputImage() == NULL) return;
    _ImageProcessingType = a_ImageProcessingType;
    start();
}

void SmoothingProcessing::run()
{
    // Set new seed for rand().
    qsrand(QTime::currentTime().msec());

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    qDebug() << "Starting image processing";

    switch(_ImageProcessingType)
    {
    case SmoothingModel::IMAGE_PROCESSING_TYPE_SALTNPEPPER:
        // Add Salt & Pepper noise to the manipulated image.
        _SmoothingModel.SetManipulatedImage(*NoiseUtils::GetSaltNPepperNoiseImage(_SmoothingModel.GetManipulatedImage(), _SmoothingModel.GetSaltNPepperDensity()));
        break;
    case SmoothingModel::IMAGE_PROCESSING_TYPE_GAUSSIAN:
        // Add Gaussian noise to the manipulated image.
        _SmoothingModel.SetManipulatedImage(*NoiseUtils::GetGaussianNoiseImage(_SmoothingModel.GetManipulatedImage(), _SmoothingModel.GetGaussianDeviation()));
        break;
    case SmoothingModel::IMAGE_PROCESSING_TYPE_UNIFORM:
        // Add Uniform noise to the manipulated image.
        _SmoothingModel.SetManipulatedImage(*NoiseUtils::GetUniformNoiseImage(_SmoothingModel.GetManipulatedImage(), _SmoothingModel.GetUniformMagnitude()));
        break;
    case SmoothingModel::IMAGE_PROCESSING_TYPE_AVERAGE:
        // Generate averaged image.
        _SmoothingModel.SetAveragedImage(*SmoothingUtils::GetAveragedImage(_SmoothingModel.GetManipulatedImage(), _SmoothingModel.GetResolution()));
        // Generate absolute averaged image.
        _SmoothingModel.SetAbsoluteAveragedImage(*ImageUtils::GetAbsoluteImage(_SmoothingModel.GetManipulatedImage(), _SmoothingModel.GetAveragedImage()));
        break;
    case SmoothingModel::IMAGE_PROCESSING_TYPE_MEDIAN:
        // Generate median image.
        _SmoothingModel.SetMedianImage(*SmoothingUtils::GetMedianImage(_SmoothingModel.GetManipulatedImage(), _SmoothingModel.GetResolution()));
        // Generate absolute median image.
        _SmoothingModel.SetAbsoluteMedianImage(*ImageUtils::GetAbsoluteImage(_SmoothingModel.GetManipulatedImage(), _SmoothingModel.GetMedianImage()));
        break;
    }


    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    qDebug() << "Image processed in: "
             << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
             << "us";
}
