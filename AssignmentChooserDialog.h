#ifndef ASSIGNMENTCHOOSERDIALOG_H
#define ASSIGNMENTCHOOSERDIALOG_H

#include <QDialog>

#include "Smoothing/View/SmoothingView.h"
#include "Smoothing/Model/SmoothingModel.h"
#include "Smoothing/Controller/SmoothingController.h"
#include "Smoothing/SmoothingProcessing.h"

#include "ObjectSegmentation/View/ObjectSegmentationView.h"
#include "ObjectSegmentation/Model/ObjectSegmentationModel.h"
#include "ObjectSegmentation/Controller/ObjectSegmentationController.h"
#include "ObjectSegmentation/ObjectSegmentationProcessing.h"

#include "EdgeDetection/Model/EdgeDetectionModel.h"
#include "EdgeDetection/View/EdgeDetectionView.h"


#include "Morphology/Model/MorphologyModel.h"
#include "Morphology/View/MorphologyView.h"

#include "RLE/Model/RleModel.h"
#include "RLE/View/RleView.h"

#include "FeatureExtraction/Model/FeModel.h"
#include "FeatureExtraction/View/FeView.h"

#include "DiceTossing//Model/DiceTossingModel.h"
#include "DiceTossing/View/DiceTossingView.h"

namespace Ui {
class AssignmentChooserDialog;
}

class AssignmentChooserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AssignmentChooserDialog(QWidget *parent = 0);
    ~AssignmentChooserDialog();

public slots:
    void StartHistogram(bool);
    void StartObjectSegmentation(bool);
    void StartSmoothing(bool);
    void StartEdgeDetection(bool);
    void StartMorphology(bool);
    void StartRle();
    void StartFe();
    void StartDiceTossing();

private:
    Ui::AssignmentChooserDialog *ui;


    SmoothingModel *smoothingModel;
    SmoothingController *smoothingController;
    SmoothingProcessing *smoothingProcessing;
    SmoothingView *smoothingView;

    ObjectSegmentationModel *objectSegmentationModel;
    ObjectSegmentationController *objectSegmentationController;
    ObjectSegmentationProcessing *objectSegmentationProcessing;
    ObjectSegmentationView *objectSegmentationView;

    EdgeDetectionModel *edgeDetectionModel;
    EdgeDetectionView *edgeDetectionView;

    MorphologyModel *morphologyModel;
    MorphologyView * morphologyView;

    RleModel *rleModel;
    RleView * rleView;

    FeModel *feModel;
    FeView * feView;

    DiceTossingModel *diceTossingModel;
    DiceTossingView *diceTossingView;
};

#endif // ASSIGNMENTCHOOSERDIALOG_H
