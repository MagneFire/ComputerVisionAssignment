#include "EdgeDetectionModel.h"
#include "../EdgeDetectionProcessing.h"

EdgeDetectionModel::EdgeDetectionModel():
    _InputImage(NULL),
    _SmoothImage(NULL),
    _SXImage(NULL),
    _SYImage(NULL),
    processing(new EdgeDetectionProcessing(this))
{

}

void EdgeDetectionModel::SetInputImage(QImage & input)
{
    int width = input.width();
    int height = input.height();
    _InputImage = &input;
    emit UpdateInputImage();
    QImage *tmpImage;
    if (width > height) tmpImage = new QImage(input.scaledToWidth(_scale));
    else _ScaledImage = tmpImage = new QImage(input.scaledToHeight(_scale));
    SetScaledImage(*tmpImage);
}

void EdgeDetectionModel::SetScaledImage(QImage & img)
{
    _ScaledImage = &img;
    emit UpdateScaledImage();
    processing->DoImageProcessing(EdgeDetectionModel::IMAGE_PROCESSING_TYPE_AVERAGE);
}

void EdgeDetectionModel::SetSmoothImage(QImage & img)
{
    _SmoothImage = &img;
    emit UpdateSmoothImage();
    processing->DoImageProcessing(EdgeDetectionModel::IMAGE_PROCESSING_TYPE_SX);
}

void EdgeDetectionModel::SetSXImage(QImage & img)
{
    _SXImage = &img;
    emit UpdateSXImage();
    processing->DoImageProcessing(EdgeDetectionModel::IMAGE_PROCESSING_TYPE_SY);
}
void EdgeDetectionModel::SetSYImage(QImage & img)
{
    _SYImage = &img;
    emit UpdateSYImage();
    processing->DoImageProcessing(EdgeDetectionModel::IMAGE_PROCESSING_TYPE_GRADIENT);
}

void EdgeDetectionModel::SetGradientImage(QImage &img)
{
    _GradientImage = &img;
    emit UpdateGradientImage();
    processing->DoImageProcessing(EdgeDetectionModel::IMAGE_PROCESSING_TYPE_THRESHOLD);
}

void EdgeDetectionModel::SetThresholdImage(QImage &img)
{
    _ThresholdImage = &img;
    emit UpdateThresholdImage();
}
