#include "EdgeDetectionProcessing.h"

EdgeDetectionProcessing::EdgeDetectionProcessing(EdgeDetectionModel * model) :
      model(*model)
{

}

void EdgeDetectionProcessing::DoImageProcessing(EdgeDetectionModel::ImageProcessingType a_ImageProcessingType)
{
    if (model.GetInputImage() == NULL) return;
    _ImageProcessingType = a_ImageProcessingType;
    if (!isRunning())
    {
        start();
    }
}

void EdgeDetectionProcessing::run()
{
    // Set new seed for rand().
    qsrand(QTime::currentTime().msec());
    double array[9];
    int total;
    EdgeDetectionModel::ImageProcessingType type = _ImageProcessingType;

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    qDebug() << "Starting image processing";

    do
    {
        // Update image processing type.
        type = _ImageProcessingType;
        switch(_ImageProcessingType)
        {
        case EdgeDetectionModel::IMAGE_PROCESSING_TYPE_AVERAGE:
            // Set the average filter.
            array[0] = 1.0; array[1] = 1.0; array[2] = 1.0;
            array[3] = 1.0; array[4] = 1.0; array[5] = 1.0;
            array[6] = 1.0; array[7] = 1.0; array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set averaged smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case EdgeDetectionModel::IMAGE_PROCESSING_TYPE_GAUSSIAN:
            // Set the Gaussian filter.
            array[0] = 1.0; array[1] = 2.0; array[2] = 1.0;
            array[3] = 2.0; array[4] = 4.0; array[5] = 2.0;
            array[6] = 1.0; array[7] = 2.0; array[8] = 1.0;
            total = Utils::SumArray((double*)&array, 9);
            for (uint8_t i=0; i < 9; ++i)
                array[i] /= (double)total;
            // Set median smoothed image.
            model.SetSmoothImage(*SmoothingUtils::GetFilteredImage(model.GetScaledImage(), array, 9));
            break;
        case EdgeDetectionModel::IMAGE_PROCESSING_TYPE_SX:
            // Set Sobel X operator/filter.
            array[0] = -1.0; array[1] = -2.0; array[2] = -1.0;
            array[3] =  0.0; array[4] =  0.0; array[5] =  0.0;
            array[6] =  1.0; array[7] =  2.0; array[8] =  1.0;
            // Set Sobel X image.
            model.SetSXImage(*SmoothingUtils::GetFilteredImage(model.GetSmoothImage(), array, 9));
            break;
        case EdgeDetectionModel::IMAGE_PROCESSING_TYPE_SY:
            // Set Sobel Y operator/filter.
            array[0] = -1.0; array[1] =  0.0; array[2] =  1.0;
            array[3] = -2.0; array[4] =  0.0; array[5] =  2.0;
            array[6] = -1.0; array[7] =  0.0; array[8] =  1.0;
            // Set Sobel Y image.
            model.SetSYImage(*SmoothingUtils::GetFilteredImage(model.GetSmoothImage(), array, 9));
            break;
        case EdgeDetectionModel::IMAGE_PROCESSING_TYPE_GRADIENT:
            // Set gradient image.
            model.SetGradientImage(*SmoothingUtils::GetGradientImage(model.GetSXImage(), model.GetSYImage()));
            break;
        case EdgeDetectionModel::IMAGE_PROCESSING_TYPE_THRESHOLD:
            model.SetThresholdImage(*SmoothingUtils::GetThresholdImage(model.GetGradientImage(), false, model.GetThreshold()));
        }
    } while (type != _ImageProcessingType);


    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    qDebug() << "Image processed in: "
             << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
             << "us";
}
