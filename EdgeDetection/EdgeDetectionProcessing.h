#ifndef EDGEDETECTIONPROCESSING_H
#define EDGEDETECTIONPROCESSING_H

#include <QDebug>
#include <QObject>
#include <QThread>
#include <QTime>

#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include "Model/EdgeDetectionModel.h"
#include "../Util/SmoothingUtils.h"
#include "../Util/Utils.h"
//class EdgeDetectionModel;

class EdgeDetectionProcessing : public QThread
{
    Q_OBJECT
public:
    explicit EdgeDetectionProcessing(EdgeDetectionModel * model);
public slots:
    void DoImageProcessing(EdgeDetectionModel::ImageProcessingType);
private:
    /// Thread can only be started using given functions.
    void run();
    EdgeDetectionModel & model;
    EdgeDetectionModel::ImageProcessingType _ImageProcessingType;
};

#endif // EDGEDETECTIONPROCESSING_H
